﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class copiaArmasGeneralPorSiAcaso : MonoBehaviour
{
    //Copia de por si acaso
    [SerializeField]
    public Arma arma;
    public Camera camara;
    public AudioClip sonido;
    public Text TextoBalas;
    public Image imagen;

    private void Awake()
    {
        arma.balas = arma.cargador;
        TextoBalas.text = arma.balas + "/" + arma.balasCinto;
        arma.disparando = false;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && arma.disparando == false)
        {
            this.GetComponent<Animator>().SetBool("Apuntar", false);
            camara.nearClipPlane = 0.3f;
            camara.fieldOfView = 60;
            arma.disparando = true;
            Shoot();
            this.GetComponent<Animator>().SetTrigger("Disparo");
            StartCoroutine(CambioEstadoDisp(arma.tiempoEntreDisparos));
        }
        else if (Input.GetMouseButton(1) && arma.disparando == false)
        {
            camara.fieldOfView = 15;
            camara.nearClipPlane = 0.2f;
            this.GetComponent<Animator>().SetBool("Apuntar", true);
        }
        else
        {
            camara.nearClipPlane = 0.3f;
            camara.fieldOfView = 60;
            this.GetComponent<Animator>().SetBool("Apuntar", false);
        }
        if ((arma.balas == 0 || Input.GetButtonDown("Reload")) && arma.disparando == false && arma.balasCinto > 0)
        {
            this.GetComponent<Animator>().SetTrigger("Recarga");
            StartCoroutine(CambioEstadoDisp(1f));
            arma.disparando = true;
            if (arma.balasCinto < arma.cargador && arma.balasCinto > 0)
            {
                if (arma.balas > arma.balasCinto)
                {
                    while (arma.balas < arma.cargador)
                    {
                        if (arma.balasCinto <= 0)
                        {
                            break;
                        }
                        else
                        {
                            arma.balasCinto--;
                            arma.balas++;
                        }
                    }
                }
                else
                {
                    arma.balas += arma.balasCinto;
                    arma.balasCinto = 0;
                }
            }
            else
            {
                arma.balasCinto -= (arma.cargador - arma.balas);
                arma.balas = arma.cargador;
            }
        }
        TextoBalas.text = arma.balas + "/" + arma.balasCinto;
    }

    private void Shoot()
    {
        if (arma.balas > 0)
        {
            for (int i = 0; i < arma.balasPorDisparo; i++)
            {
                Vector3 newForward = new Vector3(-this.transform.forward.x + UnityEngine.Random.Range(-0.05f, 0.05f), -this.transform.forward.y + UnityEngine.Random.Range(-0.05f, 0.05f), -this.transform.forward.z + UnityEngine.Random.Range(-0.05f, 0.05f));
                if (Physics.Raycast(this.transform.position, newForward, out RaycastHit hit, arma.range))
                {
                    Debug.DrawRay(this.transform.position, newForward, Color.red);
                    print(hit.transform.name);
                    if (hit.transform.tag == "Disparable")
                    {
                        hit.transform.gameObject.GetComponent<Disparable>().daño(arma.damage);
                        hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * arma.momentum, hit.point);
                    }
                    GameObject efectoDelArma = Instantiate(arma.efecto);
                    efectoDelArma.transform.position = hit.point;
                    Destroy(efectoDelArma, 1f);
                }
            }
            arma.balas--;
        }
    }

    public IEnumerator CambioEstadoDisp(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        arma.disparando = !arma.disparando;
    }
}
