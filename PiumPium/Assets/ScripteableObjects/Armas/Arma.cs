﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Arma", menuName = "ScriptableObjects/Arma", order = 2)]
public class Arma : ScriptableObject
{
    //Variables
    [SerializeField]
    public new string nombre;
    [Space]
    public float damage;
    public float range;
    public float momentum;
    public float fov;
    public float distVision;
    [Space]
    [Space]
    public int numDisp;
    public bool dispersion;
    public float rangoDispersion;
    public bool automatico;
    public bool disparando = false;
    public float tiempoEntreDisparos;
    //public float cadencia ;
    [Space]
    [Space]
    public int balasPorDisparo;
    public int balasCinto;
    public int cargador;
    public int balas;
    [Space]
    [Space]
    public GameObject efecto;
    //public Text TextoBalas;
    //public Image imagen;
}
