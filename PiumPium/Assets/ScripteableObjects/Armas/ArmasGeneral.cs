﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ArmasGeneral : MonoBehaviour
{
    [SerializeField]
    public Arma arma;
    public Camera camara;
    public AudioClip sonido;
    public Text TextoBalas;
    public Image imagen;
    int balasInvent;


    //pones las balas y el texto
    private void Awake()
    {
        balasInvent=arma.balasCinto;
        arma.balas = arma.cargador;
        TextoBalas.text = arma.balas + "/" + arma.balasCinto;
        arma.disparando = false;
    }

    void Update()
    {
        //disparar
        if (arma.automatico)
        {
            if (Input.GetMouseButton(0) && arma.disparando == false)
            {
                arma.disparando = true;
                Shoot();
                this.GetComponent<Animator>().SetBool("Disparo", true);
                StartCoroutine(CambioEstadoDisp(arma.tiempoEntreDisparos));
            }
            else if (!Input.GetMouseButton(0) || arma.balas == 0)
            {
                this.GetComponent<Animator>().SetBool("Disparo", false);
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0) && arma.disparando == false)
            {
                arma.disparando = true;
                Shoot();
                this.GetComponent<Animator>().SetBool("Disparo", true);
                StartCoroutine(CambioEstadoDisp(arma.tiempoEntreDisparos));
            }
            else if (!Input.GetMouseButtonDown(0) || arma.balas == 0)
            {
                this.GetComponent<Animator>().SetBool("Disparo", false);
            }
        }
        //apuntar
        if (Input.GetMouseButton(1))
        {
            camara.fieldOfView = arma.fov;
            camara.nearClipPlane = 0.2f;
            this.GetComponent<Animator>().SetBool("Apuntar", true);
        }
        else
        {
            camara.nearClipPlane = arma.distVision;
            camara.fieldOfView = 60;
            this.GetComponent<Animator>().SetBool("Apuntar", false);
        }


        //Recarga
        if ((arma.balas == 0 || Input.GetButtonDown("Reload")) && arma.disparando == false && balasInvent > 0)
        {
            this.GetComponent<Animator>().SetTrigger("Recarga");
            StartCoroutine(CambioEstadoDisp(1.5f));
            arma.disparando = true;
            if (balasInvent < arma.cargador && balasInvent > 0)
            {
                if (arma.balas > balasInvent)
                {
                    while (arma.balas < arma.cargador)
                    {
                        if (balasInvent <= 0)
                        {
                            break;
                        }
                        else
                        {
                            balasInvent--;
                            arma.balas++;
                        }
                    }
                }
                else
                {
                    arma.balas += balasInvent;
                    balasInvent = 0;
                }
            }
            else
            {
                balasInvent -= (arma.cargador - arma.balas);
                arma.balas = arma.cargador;
            }
        }
        TextoBalas.text = arma.balas + "/" + balasInvent;
    }


    //disparo con raycast
    private void Shoot()
    {
        if (arma.balas > 0)
        {
            for (int i = 0; i < arma.balasPorDisparo; i++)
            {
                Vector3 newForward = new Vector3(-this.transform.forward.x + UnityEngine.Random.Range(-arma.rangoDispersion, arma.rangoDispersion), -this.transform.forward.y + UnityEngine.Random.Range(-arma.rangoDispersion, arma.rangoDispersion), -this.transform.forward.z + UnityEngine.Random.Range(-arma.rangoDispersion, arma.rangoDispersion));
                if (Physics.Raycast(this.transform.position, newForward, out RaycastHit hit, arma.range))
                {
                    //Debug.DrawRay(this.transform.position, newForward, Color.red);
                    //print(hit.transform.name);
                    if (hit.transform.tag == "Disparable")
                    {
                        hit.transform.gameObject.GetComponent<Disparable>().daño(arma.damage);
                        hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * arma.momentum, hit.point);
                    }
                    GameObject efectoDelArma = Instantiate(arma.efecto);
                    efectoDelArma.transform.position = hit.point;
                    Destroy(efectoDelArma, 1f);
                }
            }
            arma.balas--;
        }
    }


    //cambial el estado de disparando a no disparando para volver a poder disparar
    public IEnumerator CambioEstadoDisp(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        arma.disparando = !arma.disparando;
    }
}
