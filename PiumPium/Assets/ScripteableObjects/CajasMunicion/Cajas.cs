﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Caja", menuName = "ScriptableObjects/Caja", order = 1)]
public class Cajas : ScriptableObject
{
    //Variables
    public int calliber;
    public int numBullets;
}

