﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CajasGeneral : MonoBehaviour
{
    [SerializeField]
    public Cajas caja;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Equals("RigidBodyFPSController"))
        {
            if (caja.calliber == 14)
            {
                if (GameObject.Find("AK74") != null)
                {
                    //Añade las balas al jugador
                    var Ak = GameObject.Find("AK74");
                    Ak.GetComponent<ArmasGeneral>().arma.balasCinto += 30;
                    Destroy(this.gameObject);
                }
                else
                {
                    Debug.Log("Arma equivocada");
                }

            }
            if (caja.calliber == 8)
            {
                if (GameObject.Find("Bennelli_M4"))
                {
                    //Añade las balas al jugador
                    var escopeta = GameObject.Find("Bennelli_M4");
                    //Debug.Log(escopeta.GetComponent<ArmasGeneral>().arma.balasCinto);
                    escopeta.GetComponent<ArmasGeneral>().arma.balasCinto += 10;
                    //Debug.Log(escopeta.GetComponent<ArmasGeneral>().arma.balasCinto);
                    Destroy(this.gameObject);
                }
                else
                {
                    Debug.Log("Arma equivocada");
                }
            }
            if (caja.calliber == 1)
            {
                collision.gameObject.GetComponent<Jugador>().granadas += 3;
                Destroy(this.gameObject);
            }
        }
    }
}
