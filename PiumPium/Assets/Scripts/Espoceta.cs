﻿using UnityEngine;
using UnityEngine.UI;

public class Espoceta : MonoBehaviour
{
    //No mirar, que esto era la version 1 de las armas
    float damage = 10f;
    float range = 20f;
    float momentum = 200f;
    public Camera camara;
    public GameObject bengala;
    bool disparando = false;
    public int balasCinto;
    public int cargador = 8;
    public int balas;
    public Text textobalsa;

    private void Awake()
    {
        balas = cargador;
        textobalsa.text = balas + "/" + balasCinto;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && disparando == false)
        {
            this.GetComponent<Animator>().SetBool("Apuntar", false);
            camara.nearClipPlane = 0.3f;
            camara.fieldOfView = 60;
            disparando = true;
            Shoot();
            this.GetComponent<Animator>().SetTrigger("Disparo");
            Invoke("cambioEstadoDisp", 1f);
        }
        else if (Input.GetMouseButton(1) && disparando == false)
        {
            camara.fieldOfView = 15;
            camara.nearClipPlane = 0.2f;
            this.GetComponent<Animator>().SetBool("Apuntar", true);
        }
        else
        {
            camara.nearClipPlane = 0.3f;
            camara.fieldOfView = 60;
            this.GetComponent<Animator>().SetBool("Apuntar", false);
        }
        if ((balas == 0 || Input.GetButtonDown("Reload")) && disparando == false && balasCinto > 0)
        {
            this.GetComponent<Animator>().SetTrigger("Recarga");
            Invoke("cambioEstadoDisp", 1f);
            disparando = true;
            if (balasCinto < cargador && balasCinto > 0)
            {
                if (balas > balasCinto)
                {
                    while (balas < cargador)
                    {
                        if (balasCinto <= 0)
                        {
                            break;
                        }
                        else
                        {
                            balasCinto--;
                            balas++;
                        }
                    }
                }
                else
                {
                    balas += balasCinto;
                    balasCinto = 0;
                }
            }
            else
            {
                balasCinto -= (cargador - balas);
                balas = cargador;
            }
        }
        textobalsa.text = balas + "/" + balasCinto;
        Debug.Log(balas);
    }
    private void CambioEstadoDisp()
    {
        disparando = !disparando;
    }
    private void Shoot()
    {
        if (balas > 0)
        {
            for (int i = 0; i < 12; i++)
            {
                Vector3 newForward = new Vector3(-this.transform.forward.x + UnityEngine.Random.Range(-0.05f, 0.05f), -this.transform.forward.y + UnityEngine.Random.Range(-0.05f, 0.05f), -this.transform.forward.z + UnityEngine.Random.Range(-0.05f, 0.05f));
                if (Physics.Raycast(this.transform.position, newForward, out RaycastHit hit, range))
                {
                    Debug.DrawRay(this.transform.position, newForward, Color.red);
                    print(hit.transform.name);
                    if (hit.transform.tag == "Disparable")
                    {
                        hit.transform.gameObject.GetComponent<Disparable>().daño(damage);
                        hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * momentum, hit.point);
                    }
                    GameObject newBengala = Instantiate(bengala);
                    newBengala.transform.position = hit.point;
                    Destroy(newBengala, 1f);
                }
            }
            balas--;
        }
    }
}
