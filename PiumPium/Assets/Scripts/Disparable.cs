﻿using UnityEngine;
using UnityEngine.Events;

public class Disparable : MonoBehaviour
{
    public float hp = 50f;
    public GameObject ragdoll;
    public UnityEvent OnFenecimiento;
    
    //Funcion de los enemigos 
    public void daño(float damage)
    {
        hp -= damage;
        if (hp <= 0)
        {
            if (this.name == "Enemy")
            {
                GameObject cosoRagdoll = Instantiate(ragdoll, this.transform.position, this.transform.rotation);
                OnFenecimiento.Invoke();
                Destroy(this.gameObject);

            }
            Destroy(this.gameObject);
        }
    }

}
