﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Powerup : MonoBehaviour
{
    float speed = 50f;
    public UnityEvent Coger;
    public AudioClip coin;

    //Movimiento de la moneda
    private void Update()
    {
        transform.Rotate(Vector3.up * speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        //Augmenta la variable de las monedas del jugador
        if (other.gameObject.name.Equals("RigidBodyFPSController"))
        {
            Coger.Invoke();
            AudioSource.PlayClipAtPoint(coin, transform.position);
            Destroy(this.gameObject);
        }
    }


}
