﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Puerta : MonoBehaviour
{
    public int Luces = 0;
    public UnityEvent OnPuertaAbierta;

    void Update()
    {
        //Activa la animación
        if (Luces == 3)
        {
            GetComponent<Animator>().SetBool("On", true);
            //Activaria otro evento, pero ya para la otra entrega
            OnPuertaAbierta.Invoke();
        }
    }

    //Cuando pasa el evento augmenta el numero de luces
    public void AugmLuces()
    {
        Luces++;
    }

}
