﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class SujetosPruebas : MonoBehaviour
{
    public bool rojo = false;
    public NavMeshAgent agent;
    public UnityEvent cambio;
    public GameObject Rojo;
    public GameObject Azul;


    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (rojo)
        {
            agent.SetDestination(Rojo.transform.position);
        }
        else
        {
            agent.SetDestination(Azul.transform.position);
        }
    }


    public void cambiarAzul()
    {
        Debug.Log("cambioazul");
        rojo = false;
    }

    public void cambiarRojo()
    {
        rojo = true;
        Debug.Log("cambiorojo");
    }

}

