﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class enemigue : MonoBehaviour
{
    public bool huir = false;
    public NavMeshAgent agent;
    public UnityEvent Fear;
    public GameObject player;
    public float distancia = 4f;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        distancia = Vector3.Distance(transform.position, player.transform.position);

        if (huir)
        {
            Vector3 coso = transform.position - player.transform.position;

            Vector3 cosonuevo = transform.position + coso;

            agent.SetDestination(cosonuevo);
        }
        else
        {
            agent.SetDestination(GameObject.Find("RigidBodyFPSController").transform.position);
        }
        
    }


    public void cambiarEstado()
    {
        huir = true;
        StartCoroutine(CEstado(10));
    }

    public IEnumerator CEstado(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        huir = false;
    }
}
