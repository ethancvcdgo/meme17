﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Girar : MonoBehaviour
{
    float speed = 50f;
    public AudioClip coin;

    //Movimiento de la moneda
    private void Update()
    {
        transform.Rotate(Vector3.up * speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        //Augmenta la variable de las monedas del jugador
        if (other.gameObject.name.Equals("RigidBodyFPSController"))
        {
            other.gameObject.GetComponent<Jugador>().monedas++;
            AudioSource.PlayClipAtPoint(coin, transform.position);
            Destroy(this.gameObject);
        }
    }


}
