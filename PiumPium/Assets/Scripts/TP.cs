﻿using UnityEngine;
using System.Collections;

public class TP : MonoBehaviour
{
    private RaycastHit Hit;
    public AudioClip ClipTp;
    public float rangodelTP = 100;
    public GameObject Efecto;
    public bool tp;

    //Esto coge el objeto que el jugador este mirando y le hace un raycast
    private GameObject GetLookedAtObject()
    {
        Vector3 origen = this.transform.position;
        Vector3 direccion = Camera.main.transform.forward;

        if (Physics.Raycast(origen, direccion, out Hit, rangodelTP))
        {
            return Hit.collider.gameObject;
        }
        else
        {
            return null;
        }
    }

    //Cambia la posicion del jugador;
    private void TpEnEsaPosicion()
    {
        //Debug.DrawRay(this.transform.position, Hit.point, Color.red);
        transform.position = Hit.point + Hit.normal * 1.5f;
        if (ClipTp != null)
        {
            AudioSource.PlayClipAtPoint(ClipTp, transform.position);
        }
    }
    void Update()
    {
        if (Input.GetKey("f"))
        {

            GameObject efectoDelTP = Instantiate(Efecto);
            efectoDelTP.transform.position = Hit.point + Hit.normal * 1.5f;
            StartCoroutine(EliminarEfecto(0.1f, efectoDelTP));
            if (GetLookedAtObject() != null)
            {
                tp = true;
            }
            else
            {
                tp = false;
            }
        }
        if (Input.GetKeyUp("f"))
        {
            if (tp == true)
            {
                TpEnEsaPosicion();
            }
            
        }
    }

    public IEnumerator EliminarEfecto(float tiempo, GameObject efecto)
    {
        yield return new WaitForSeconds(tiempo);
        Destroy(efecto);
    }
}