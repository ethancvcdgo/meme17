﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    public GameObject ObjetosParte2;
    public GameObject ObjetosParte3;
    public int cubosRojos = 0;
    public bool haSusedido = false;
    public int ContadordeEnemigos = 0;
    public GameObject MonedaIntermedia;
    public GameObject MonedaFinal;
    public List<enemigue> enemigosNavMeshZonaInicial;
    public List<SujetosPruebas> enemigosNavMeshCamaradePruebas;

    public bool Activo = false;

    private void Update()
    {
        if (cubosRojos >= 10)
        {
            if (haSusedido == false)
            {
                Parte2();
            }
            if (ContadordeEnemigos == 8)
            {
                if (Activo == false)
                {
                    MonedaIntermedia.gameObject.SetActive(true);
                    Activo = true;
                    Parte3();
                }
            }
        }
    }

    private void Parte3()
    {
        if (ObjetosParte3.transform.childCount != 0)
        {
            int total = ObjetosParte3.transform.childCount;
            for (int i = 0; i < total; i++)
            {
                var objeto = ObjetosParte3.transform.GetChild(i);
                if (objeto.transform.childCount != 0)
                {
                    var total2 = objeto.transform.childCount;
                    for (int j = 0; j < total2; j++)
                    {
                        var objeto2 = ObjetosParte3.transform.GetChild(i).GetChild(j);
                        objeto2.gameObject.SetActive(true);
                    }
                }
                objeto.gameObject.SetActive(true);
            }
            MonedaFinal.gameObject.SetActive(true);
        }

    }

    private void Parte2()
    {
        haSusedido = true;
        if (ObjetosParte2.transform.childCount != 0)
        {
            int total = ObjetosParte2.transform.childCount;
            for (int i = 0; i < total; i++)
            {
                var objeto = ObjetosParte2.transform.GetChild(i);
                objeto.gameObject.SetActive(true);
            }
        }
    }

    public void Cubos()
    {
        cubosRojos++;
    }

    public void EnemigosFenecidos()
    {
        ContadordeEnemigos++;
    }

    public void Fear()
    {
        foreach (var enemigo in enemigosNavMeshZonaInicial)
        {
            enemigo.cambiarEstado();
        }
    }

    public void CambiarAzul()
    {
        foreach (var enemigo in enemigosNavMeshCamaradePruebas)
        {
            enemigo.cambiarAzul();
        }
    }

    public void CambiarRojo()
    {
        foreach (var enemigo in enemigosNavMeshCamaradePruebas)
        {
            enemigo.cambiarRojo();
        }
    }
}
