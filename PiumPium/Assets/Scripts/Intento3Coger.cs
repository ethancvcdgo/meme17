﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Intento3Coger : MonoBehaviour
{
    private Material m_Material;
    public float hp = 50f;
    public float FuerzaLanzar = 1000;
    public Transform PosJugador;
    public Transform PosCamara;
    public bool touched = false;
    public bool hasPlayer = false;
    public bool beingCarried = false;
    public AudioClip sonido;
    public UnityEvent Cambio;

    private void Start()
    {
        //Asigna el componente a la variable
        m_Material = GetComponent<Renderer>().material;
    }

    void Update()
    {
        //Setea la distancia minima del jugador al objeto
        float distancia = Vector3.Distance(gameObject.transform.position, PosJugador.position);
        if (distancia <= 3f)
        {
            hasPlayer = true;
        }
        else
        {
            hasPlayer = false;
        }

        //Use = e, Va cambiando segun toque alguna cosa o se pulse algun boton del raton, sirve para coger un objeto y poder llevarlo de x a y
        if (hasPlayer && Input.GetButtonDown("Use") && !beingCarried)
        {
            GetComponent<Rigidbody>().isKinematic = true;
            transform.parent = PosCamara;
            beingCarried = true;
        }
        if (beingCarried)
        {
            if (touched)
            {
                GetComponent<Rigidbody>().isKinematic = false;
                transform.parent = null;
                beingCarried = false;
                touched = false;
            }
            if (Input.GetMouseButtonDown(0))
            {
                GetComponent<Rigidbody>().isKinematic = false;
                transform.parent = null;
                GetComponent<Rigidbody>().AddTorque(PosCamara.forward * FuerzaLanzar * 15);
                beingCarried = false;

            }
            else if (Input.GetMouseButtonDown(1))
            {
                GetComponent<Rigidbody>().isKinematic = false;
                transform.parent = null;
                beingCarried = false;
            }
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (beingCarried)
        {
            touched = true;
        }
    }

    //Funcion para el evento de las granadas
    public void daño(float damage)
    {
        hp -= damage;
        if (hp <= 0)
        {
            m_Material.color = Color.red;
            Cambio.Invoke();
        }
    }
}

