﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Boton3D : MonoBehaviour
{
    private Material m_Material;
    public UnityEvent OnClick;
    public Transform PosJugador;

    // Use this for initialization
    void Start()
    {
        //Asigna el componente a la variable
        m_Material = GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        //Setea la distancia minima del jugador al objeto
        float distancia = Vector3.Distance(gameObject.transform.position, PosJugador.position);
        if (distancia <= 3f)
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit Hit;
            if (Input.GetMouseButtonDown(0))
            {
                if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject == gameObject)
                {
                    //Cambia el color del objeto
                    m_Material.color = Color.red;
                    //Activa el evento
                    OnClick.Invoke();
                }
            }
        }
    }
}
