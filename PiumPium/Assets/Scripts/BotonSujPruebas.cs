﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BotonSujPruebas : MonoBehaviour
{
    public UnityEvent OnClick;
    public Transform PosJugador;
    public AudioClip sonido;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Setea la distancia minima del jugador al objeto
        float distancia = Vector3.Distance(gameObject.transform.position, PosJugador.position);
        if (distancia <= 4f)
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit Hit;
            if (Input.GetMouseButtonDown(0))
            {
                if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject == gameObject)
                {
                    Debug.Log("si");
                    //Activa el evento
                    AudioSource.PlayClipAtPoint(sonido, transform.position);
                    OnClick.Invoke();
                }
            }
        }
    }
}