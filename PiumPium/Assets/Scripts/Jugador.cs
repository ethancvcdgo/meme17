﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador : MonoBehaviour
{
    public List<GameObject> Armas;
    public Camera camara;
    public GameObject Granada;
    public int granadas;
    public float fuerzaLanzamiento;
    public int monedas;

    // Start is called before the first frame update
    void Start()
    {
        monedas = 0;
        granadas = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //Sistema de tooltips por consola
        if (monedas == 1)
        {
            Debug.Log("Avanza usando el TP (botón F)");
        }
        if (monedas == 3)
        {
            Debug.Log("Lanza las granadas para convertir a los cubos blancos en rojos");
        }
        if (monedas == 6)
        {
            Debug.Log("Dispara a los 8 objetivos de arriba");
        }
        if (monedas == 7)
        {
            Debug.Log("Apunta y clicka sobre los 3 botones (cubos de los postes) para abrir la puerta");
        }
        if (monedas == 8)
        {
            Debug.Log("Nice, pues ya esta.");
        }
        //Camvio de armas
        if (Input.GetKeyDown("1"))
        {
            Armas[0].gameObject.SetActive(true);
            Armas[1].gameObject.SetActive(false);
            Armas[2].gameObject.SetActive(false);
            Armas[3].gameObject.SetActive(false);
        }
        else if (Input.GetKeyDown("2"))
        {
            Armas[0].gameObject.SetActive(false);
            Armas[1].gameObject.SetActive(true);
            Armas[2].gameObject.SetActive(false);
            Armas[3].gameObject.SetActive(false);
        }
        else if (Input.GetKeyDown("3"))
        {
            Armas[0].gameObject.SetActive(false);
            Armas[1].gameObject.SetActive(false);
            Armas[2].gameObject.SetActive(true);
            Armas[3].gameObject.SetActive(false);
        }
        else if (Input.GetKeyDown("4"))
        {
            Armas[0].gameObject.SetActive(false);
            Armas[1].gameObject.SetActive(false);
            Armas[2].gameObject.SetActive(false);
            Armas[3].gameObject.SetActive(true);
        }
        //Al soltar la tecla g lanzas una granada
        else if (Input.GetKeyUp("g"))
        {
            LanzarGranada();
        }

    }

    //Instancia una granada encima del jugador
    private void LanzarGranada()
    {
        if (granadas > 0)
        {
            GameObject granada = Instantiate(Granada, transform.position + new Vector3(0, 1, 0), camara.transform.rotation);
            Rigidbody rb = Granada.GetComponent<Rigidbody>();
            granadas--;
        }
    }

}
