﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour
{
    public float damage = 50f;
    public float delay = 3f;
    public float radio = 5f;
    public float fuerzaExplosion = 700f;
    public float countdown;
    public bool haExplotado = false;
    public GameObject efectoExplosion;
    public AudioClip Expolison;


    // Start is called before the first frame update
    void Start()
    {
        countdown = delay;
        this.GetComponent<Rigidbody>().AddForce(transform.forward * 15 + this.GetComponent<Rigidbody>().velocity, ForceMode.VelocityChange);
    }

    // Update is called once per frame
    void Update()
    {
        //Cuenta atras
        countdown -= Time.deltaTime;
        if (countdown <= 0 && !haExplotado)
        {
            Explosion();
            haExplotado = true;
        }
    }

    //Explosion de la granada
    private void Explosion()
    {

        GameObject coso = Instantiate(efectoExplosion, transform.position, transform.rotation);
        AudioSource.PlayClipAtPoint(Expolison, transform.position);
        //Coge los objetos que estan en un radio desde el objeto
        Collider[] colliders = Physics.OverlapSphere(transform.position, radio);
        foreach (Collider nearbyObject in colliders)
        {
            //Por cada uno pilla el rigidbody 
            Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
            if (rb != null)
            {
                if(nearbyObject.transform.gameObject.GetComponent<Intento3Coger>() != null)
                {
                    nearbyObject.transform.gameObject.GetComponent<Intento3Coger>().daño(damage);
                }
                //Pone fuerza al rigidbody del objeto
                rb.AddExplosionForce(fuerzaExplosion, transform.position, radio);
            }
        }
        Destroy(coso, 3f);
        Destroy(gameObject);

    }
}
