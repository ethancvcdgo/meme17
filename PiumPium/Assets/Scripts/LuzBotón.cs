﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LuzBotón : MonoBehaviour
{
    private Material m_Material;
    public UnityEvent OnChange;

    // Use this for initialization
    void Start()
    {
        //Asigna el componente a la variable
        m_Material = GetComponent<Renderer>().material;
    }

    //Cambia el color del objeto
    public void CambioColor()
    {
        m_Material.color = Color.red;
        OnChange.Invoke();
    }
}
