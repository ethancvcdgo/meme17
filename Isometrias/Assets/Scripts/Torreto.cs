﻿using System.Collections.Generic;
using UnityEngine;

public class Torreto : MonoBehaviour
{
    public float FireRate;
    public int Damage;
    public int VelProyectil;
    public GameObject misile;
    private GameObject misil;
    private Queue<GameObject> enemigos = new Queue<GameObject>();
    public string tipo = "";
    public bool torreta = true;
    public int tier;

    //Va haciendo invokes de los misiles si hay enemigos, segun su fireRate.
    void Start()
    {
        InvokeRepeating("MisilePoner", 0.2f, FireRate);
    }

    private void MisilePoner()
    {
        if (enemigos.Count > 0 && enemigos.Peek() == null)
        {
            //Quita al enemigo de la Queue.
            enemigos.Dequeue();
        }
        if (enemigos.Count > 0)
        {
            //Instancia el prefab del misil.
            misil = Instantiate(misile);
            //Cambia su posición a la posición de la torreta.
            misil.transform.position = this.transform.position;
            misil.GetComponent<Misile>().enemigo = enemigos.Peek();
            misil.GetComponent<Misile>().damage = Damage;
            misil.GetComponent<Misile>().velocidad = VelProyectil;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Mira cuando un enemigo entra en el collider.
        if (collision.CompareTag("Enemigo"))
        {
            //Añade al enemigo a la Queue.
            enemigos.Enqueue(collision.gameObject);
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //Mira cuando el enemigo sale del collider.
        if (collision.CompareTag("Enemigo"))
        {
            if (enemigos.Count > 0)
            {
                //Saca al enemigo de la Queue.
                enemigos.Dequeue();
            }
        }
    }
}
