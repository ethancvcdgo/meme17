﻿using UnityEngine;

public class EnemigoVidaController : MonoBehaviour
{
    public float vida;
    public int recursos;

    //Quitar vida al chocarte
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Proyectil"))
        {
            vida -= collision.gameObject.GetComponent<Misile>().damage;
            Destroy(collision.gameObject);
            if (vida <= 0)
            {
                Destroy(this.gameObject);
                NexoController.recursos += recursos;
            }
        }
    }
}
