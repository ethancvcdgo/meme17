﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CnavasController : MonoBehaviour
{
    [Header("Torretas de Tier 1")]
    public GameObject Catapulta;
    public GameObject Canon;
    public GameObject Metralleta;
    public GameObject Balista;
    public GameObject Cristal;

    [Header("Torretas de Tier 2")]
    public GameObject CatapultaT2;
    public GameObject CanonT2;
    public GameObject MetralletaT2;
    public GameObject BalistaT2;

    [Header("Torretas de Tier 3")]
    public GameObject CatapultaT3;
    public GameObject CanonT3;
    public GameObject MetralletaT3;
    public GameObject BalistaT3;

    [Header("Booleanos de Compra/Mejora/Venta")]
    public bool torretoComprado = false;
    public bool mejorarTorreta = false;
    public bool mejorarCatapulta = false;
    public bool mejorarCanon = false;
    public bool mejorarMetralleta = false;
    public bool mejorarBalista = false;
    public bool venderTorreta = false;

    //Diferentes paneles de las torretas
    [Header("Paneles")]
    public GameObject ContornoCatapulta;
    public GameObject ContornoCanon;
    public GameObject ContornoMetralleta;
    public GameObject ContornoBalista;
    public GameObject ContornoCristal;

    [Header("Listas")]
    public List<Vector3Int> posicionesOcupadas = new List<Vector3Int>();
    public List<GameObject> objetos = new List<GameObject>();

    [Header("Tilemap")]
    public Tilemap tilemap;

    GameObject catapulta = null;
    GameObject canon = null;
    GameObject metralleta = null;
    GameObject balista = null;
    GameObject cristal = null;

    GameObject catapultaT2 = null;
    GameObject canonT2 = null;
    GameObject metralletaT2 = null;
    GameObject balistaT2 = null;

    GameObject catapultaT3 = null;
    GameObject canonT3 = null;
    GameObject metralletaT3 = null;
    GameObject balistaT3 = null;

    void Update()
    {
        //Hace cosas cuando pulsas el boton izquierdo del raton.
        if (Input.GetMouseButtonDown(0))
        {
            //Input del mouse.
            Vector2 input = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //Posición del mouse.
            Vector3Int celda = tilemap.WorldToCell(new Vector3(input.x, input.y));
            //Comprueba si la celda del tilemap existe.
            if (tilemap.GetTile(celda) != null)
            {
                //En este punto uno de los tres botones principales de un panel ha sido pulsado con anterioridad
                //Boton comprar seleccionado:
                //Mira si la torreta comprada ha sido una catapulta (ya esta el prefab instanciado).
                if (catapulta != null && catapulta.GetComponent<Torreto>().torreta == true)
                {
                    //Mira en la lista de celdas guardadas si no contiene la celda actual.
                    if (!posicionesOcupadas.Contains(celda))
                    {
                        //Se añade la celda actual a la lista de celdas.
                        posicionesOcupadas.Add(celda);
                        //Se añade el prefab actual a la lista de de objetos.
                        objetos.Add(catapulta);
                        //Cambia la posición del prefab a la celda seleccionada con el raton.
                        catapulta.transform.position = tilemap.CellToWorld(celda);
                        catapulta.GetComponent<Torreto>().torreta = false;
                        torretoComprado = false;
                    }
                }
                //Mira si la torreta comprada ha sido un cañon (ya esta el prefab instanciado).
                if (canon != null && canon.GetComponent<Torreto>().torreta == true)
                {
                    if (!posicionesOcupadas.Contains(celda))
                    {
                        //Se añade la celda actual a la lista de celdas.
                        posicionesOcupadas.Add(celda);
                        //Se añade el prefab actual a la lista de de objetos.
                        objetos.Add(canon);
                        //Cambia la posición del prefab a la celda seleccionada con el raton.
                        canon.transform.position = tilemap.CellToWorld(celda);
                        canon.GetComponent<Torreto>().torreta = false;
                        torretoComprado = false;
                    }
                }
                //Mira si la torreta comprada ha sido una metralleta (ya esta el prefab instanciado).
                if (metralleta != null && metralleta.GetComponent<Torreto>().torreta == true)
                {
                    if (!posicionesOcupadas.Contains(celda))
                    {
                        //Se añade la celda actual a la lista de celdas.
                        posicionesOcupadas.Add(celda);
                        //Se añade el prefab actual a la lista de de objetos.
                        objetos.Add(metralleta);
                        //Cambia la posición del prefab a la celda seleccionada con el raton.
                        metralleta.transform.position = tilemap.CellToWorld(celda);
                        metralleta.GetComponent<Torreto>().torreta = false;
                        torretoComprado = false;
                    }
                }
                //Mira si la torreta comprada ha sido una balista (ya esta el prefab instanciado).
                if (balista != null && balista.GetComponent<Torreto>().torreta == true)
                {
                    if (!posicionesOcupadas.Contains(celda))
                    {
                        //Se añade la celda actual a la lista de celdas.
                        posicionesOcupadas.Add(celda);
                        //Se añade el prefab actual a la lista de de objetos.
                        objetos.Add(balista);
                        //Cambia la posición del prefab a la celda seleccionada con el raton.
                        balista.transform.position = tilemap.CellToWorld(celda);
                        balista.GetComponent<Torreto>().torreta = false;
                        torretoComprado = false;
                    }
                }
                //Mira si la torreta comprada ha sido un cristal (ya esta el prefab instanciado).
                if (cristal != null && cristal.GetComponent<Cristales>().torreta == true)
                {
                    if (!posicionesOcupadas.Contains(celda))
                    {
                        //Se añade la celda actual a la lista de celdas.
                        posicionesOcupadas.Add(celda);
                        //Se añade el prefab actual a la lista de de objetos.
                        objetos.Add(cristal);
                        //Cambia la posición del prefab a la celda seleccionada con el raton.
                        cristal.transform.position = tilemap.CellToWorld(celda);
                        cristal.GetComponent<Cristales>().torreta = false;
                        torretoComprado = false;
                    }
                }
                //Boton mejorar seleccionado:
                if (mejorarTorreta == true)
                {
                    //Mira si la lista de posicionesOcupadas tiene la celda seleccionada.
                    if (posicionesOcupadas.Contains(celda))
                    {
                        //Mira el index de esa celda en la lista.
                        int i = posicionesOcupadas.IndexOf(celda);
                        //Contador.
                        int i2 = 0;
                        //Recorre la lista.
                        foreach (GameObject coso in objetos)
                        {
                            //comprueba si el contador es igual al index de la celda de la lista.
                            if (i == i2)
                            {
                                //Pasa a la funcion de mejorar la torreta.
                                MejorarTorreta(coso, celda);
                                //Cuando sale de la funcion elimina la celda de la lista de posicionesOcupadas.
                                posicionesOcupadas.Remove(celda);
                                //Elimina el objeto de la lista de objetos.
                                objetos.Remove(coso);
                                break;
                            }
                            //Contador++;
                            i2++;
                        }
                    }
                }
                //Boton vender seleccionado:
                if (venderTorreta == true)
                {
                    //Mira si la lista de posicionesOcupadas tiene la celda seleccionada.
                    if (posicionesOcupadas.Contains(celda))
                    {
                        //Mira el index de esa celda en la lista.
                        int i = posicionesOcupadas.IndexOf(celda);
                        //Contador.
                        int i2 = 0;
                        //Recorre la lista.
                        foreach (GameObject coso in objetos)
                        {
                            //comprueba si el contador es igual al index de la celda de la lista.
                            if (i == i2)
                            {
                                //Pasa a la funcion de vender la torreta.
                                VenderTorreta(coso, celda);
                                //Cuando sale de la funcion elimina la celda de la lista de posicionesOcupadas.
                                posicionesOcupadas.Remove(celda);
                                //Elimina el objeto de la lista de objetos.
                                objetos.Remove(coso);
                                break;
                            }
                            i2++;
                        }
                    }
                }
            }
        }
    }

    //Funciones de mostrar/ocultar paneles del UI.
    public void activarCatapultaT1()
    {
        ContornoCatapulta.SetActive(true);
        ContornoCanon.SetActive(false);
        ContornoMetralleta.SetActive(false);
        ContornoBalista.SetActive(false);
        ContornoCristal.SetActive(false);
    }
    public void activarCanonT1()
    {
        ContornoCatapulta.SetActive(false);
        ContornoCanon.SetActive(true);
        ContornoMetralleta.SetActive(false);
        ContornoBalista.SetActive(false);
        ContornoCristal.SetActive(false);
    }
    public void activarMetralletaT1()
    {
        ContornoCatapulta.SetActive(false);
        ContornoCanon.SetActive(false);
        ContornoMetralleta.SetActive(true);
        ContornoBalista.SetActive(false);
        ContornoCristal.SetActive(false);
    }
    public void activarBalistaT1()
    {
        ContornoCatapulta.SetActive(false);
        ContornoCanon.SetActive(false);
        ContornoMetralleta.SetActive(false);
        ContornoBalista.SetActive(true);
        ContornoCristal.SetActive(false);
    }
    public void activarCristalT1()
    {
        ContornoCatapulta.SetActive(false);
        ContornoCanon.SetActive(false);
        ContornoMetralleta.SetActive(false);
        ContornoBalista.SetActive(false);
        ContornoCristal.SetActive(true);
    }
    public void Cerrar()
    {
        ContornoCatapulta.SetActive(false);
        ContornoCanon.SetActive(false);
        ContornoMetralleta.SetActive(false);
        ContornoBalista.SetActive(false);
        ContornoCristal.SetActive(false);
    }

    // 1-Catapulta, 2-Canon, 3-Metralleta, 4-Balista, 5-Cristal.
    //Cada funcion esta designada a uno de los botones de compra de un panel del canvas, si el boton se encuentra en el panel de la catapulta, comprara una catapulta.
    public void Comprar()
    {
        ComprarTorreta(Catapulta.GetComponent<Torreto>().tipo);
    }
    public void Comprar2()
    {
        ComprarTorreta(Canon.GetComponent<Torreto>().tipo);
    }
    public void Comprar3()
    {
        ComprarTorreta(Metralleta.GetComponent<Torreto>().tipo);
    }
    public void Comprar4()
    {
        ComprarTorreta(Balista.GetComponent<Torreto>().tipo);
    }
    public void Comprar5()
    {
        ComprarTorreta(Cristal.GetComponent<Cristales>().tipo);
    }

    //Funcion que coge el tipo de la torreta, comprueba que hay recursos suficientes, los resta i llama a la funcion donde se instancia la Torreta.
    public void ComprarTorreta(string tipoTorreta)
    {
        if (torretoComprado == false)
        {
            //
            if (tipoTorreta.Equals("Catapulta") && NexoController.recursos >= 100)
            {
                //Resta los recursos.
                NexoController.recursos -= 100;
                torretoComprado = true;
                //Funcio de instanciacion de la Torreta.
                GetCatapulta();
            }
            else if (tipoTorreta.Equals("Canon") && NexoController.recursos >= 150)
            {
                //Resta los recursos.
                NexoController.recursos -= 150;
                torretoComprado = true;
                //Funcio de instanciacion de la Torreta.
                GetCanon();
            }
            else if (tipoTorreta.Equals("Metralleta") && NexoController.recursos >= 200)
            {
                //Resta los recursos.
                NexoController.recursos -= 200;
                torretoComprado = true;
                //Funcio de instanciacion de la Torreta.
                GetMetralleta();
            }
            else if (tipoTorreta.Equals("Balista") && NexoController.recursos >= 250)
            {
                //Resta los recursos.
                NexoController.recursos -= 250;
                torretoComprado = true;
                //Funcio de instanciacion de la Torreta.
                GetBalista();
            }
            else if (tipoTorreta.Equals("Cristal") && NexoController.recursos >= 75)
            {
                //Resta los recursos.
                NexoController.recursos -= 75;
                torretoComprado = true;
                //Funcio de instanciacion de la Torreta.
                GetCristal();
            }
        }
    }

    //Funcion de instanciacion de las Torretas.
    private void GetBalista()
    {
        //Input del mouse.
        Vector2 inputmouse = Input.mousePosition;
        //Posición del mouse.
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(inputmouse);
        //Instaciacion de la Balista.
        balista = Instantiate(Balista);
        balista.GetComponent<Torreto>().torreta = true;
        //Cambia la posición del prefab a la celda seleccionada con el raton.
        balista.transform.position = mousePos;
    }
    private void GetMetralleta()
    {
        //Input del mouse.
        Vector2 inputmouse = Input.mousePosition;
        //Posición del mouse.
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(inputmouse);
        //Instaciacion de la Metralleta.
        metralleta = Instantiate(Metralleta);
        metralleta.GetComponent<Torreto>().torreta = true;
        //Cambia la posición del prefab a la celda seleccionada con el raton.
        metralleta.transform.position = mousePos;
    }
    private void GetCanon()
    {
        //Input del mouse.
        Vector2 inputmouse = Input.mousePosition;
        //Posición del mouse.
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(inputmouse);
        //Instaciacion del Cañon.
        canon = Instantiate(Canon);
        canon.GetComponent<Torreto>().torreta = true;
        //Cambia la posición del prefab a la celda seleccionada con el raton.
        canon.transform.position = mousePos;
    }
    private void GetCatapulta()
    {
        //Input del mouse.
        Vector2 inputmouse = Input.mousePosition;
        //Posición del mouse.
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(inputmouse);
        //Instaciacion de la Catapulta.
        catapulta = Instantiate(Catapulta);
        catapulta.GetComponent<Torreto>().torreta = true;
        //Cambia la posición del prefab a la celda seleccionada con el raton.
        catapulta.transform.position = mousePos;
    }
    private void GetCristal()
    {
        //Input del mouse.
        Vector2 inputmouse = Input.mousePosition;
        //Posición del mouse.
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(inputmouse);
        //Instaciacion del Cristal.
        cristal = Instantiate(Cristal);
        cristal.GetComponent<Cristales>().torreta = true;
        //Cambia la posición del prefab a la celda seleccionada con el raton.
        cristal.transform.position = mousePos;
    }

    // 1-Catapulta, 2-Canon, 3-Metralleta, 4-Balista.
    //Cada funcion esta designada a uno de los botones de compra de un panel del canvas, si el boton se encuentra en el panel de la catapulta, mejorara una catapulta.
    public void Mejorar()
    {
        Mejora(Catapulta.GetComponent<Torreto>().tipo);
    }
    public void Mejorar2()
    {
        Mejora(Canon.GetComponent<Torreto>().tipo);
    }
    public void Mejorar3()
    {
        Mejora(Metralleta.GetComponent<Torreto>().tipo);
    }
    public void Mejorar4()
    {
        Mejora(Balista.GetComponent<Torreto>().tipo);
    }

    //Funcion de mejora de las Torretas.
    private void Mejora(string tipoTorreta)
    {
        if (mejorarTorreta == false)
        {
            // Mira el tipo que se le pasa a la funcion y actua en consecuencia.
            if (tipoTorreta.Equals("Catapulta") && NexoController.recursos >= 500)
            {
                //Resta los recursos.
                NexoController.recursos -= 500;
                mejorarTorreta = true;
                mejorarCatapulta = true;
            }
            if (tipoTorreta.Equals("Canon") && NexoController.recursos >= 600)
            {
                //Resta los recursos.
                NexoController.recursos -= 600;
                mejorarTorreta = true;
                mejorarCanon = true;
            }
            if (tipoTorreta.Equals("Metralleta") && NexoController.recursos >= 700)
            {
                //Resta los recursos.
                NexoController.recursos -= 700;
                mejorarTorreta = true;
                mejorarMetralleta = true;
            }
            if (tipoTorreta.Equals("Balista") && NexoController.recursos >= 800)
            {
                //Resta los recursos.
                NexoController.recursos -= 800;
                mejorarTorreta = true;
                mejorarBalista = true;
            }
        }
    }

    //
    private void MejorarTorreta(GameObject coso, Vector3Int celda)
    {
        if (torretoComprado == false && venderTorreta == false)
        {
            if (coso.GetComponent<Torreto>().tier == 1 && mejorarTorreta == true && mejorarCatapulta == true)
            {
                //Instancia la catapulta de tier2.
                catapultaT2 = Instantiate(CatapultaT2);
                //Mira la posicion en x, y del Gameobject que se le pasa a la funcion y se los pone al prefab instaciado. 
                catapultaT2.transform.position = new Vector3(coso.transform.position.x, coso.transform.position.y);
                //Añade la celda del mapa en la que esta el prefab instanciado.
                posicionesOcupadas.Add(celda);
                //Añade el prefab instanciado a la lista de objetos(GameObject).
                objetos.Add(catapultaT2);
                //Destruye el GameObject que se le pasa a la funcion.
                Destroy(coso);
                mejorarTorreta = false;
                mejorarCatapulta = false;

            }
            else if (coso.GetComponent<Torreto>().tier == 2 && mejorarTorreta == true && mejorarCatapulta == true)
            {
                //Instancia la catapulta de tier3.
                catapultaT3 = Instantiate(CatapultaT3);
                //Mira la posicion en x, y del Gameobject que se le pasa a la funcion y se los pone al prefab instaciado. 
                catapultaT3.transform.position = new Vector3(coso.transform.position.x, coso.transform.position.y);
                //Añade la celda del mapa en la que esta el prefab instanciado.
                posicionesOcupadas.Add(celda);
                //Añade el prefab instanciado a la lista de objetos(GameObject).
                objetos.Add(catapultaT3);
                //Destruye el GameObject que se le pasa a la funcion.
                Destroy(coso);
                mejorarTorreta = false;
                mejorarCatapulta = false;
            }

            if (coso.GetComponent<Torreto>().tier == 1 && mejorarTorreta && mejorarCanon == true)
            {
                //Instancia el cañon de tier2.
                canonT2 = Instantiate(CanonT2);
                //Mira la posicion en x, y del Gameobject que se le pasa a la funcion y se los pone al prefab instaciado. 
                canonT2.transform.position = new Vector3(coso.transform.position.x, coso.transform.position.y);
                //Añade la celda del mapa en la que esta el prefab instanciado.
                posicionesOcupadas.Add(celda);
                //Añade el prefab instanciado a la lista de objetos(GameObject).
                objetos.Add(canonT2);
                //Destruye el GameObject que se le pasa a la funcion.
                Destroy(coso);
                mejorarTorreta = false;
                mejorarCanon = false;
            }
            else if (coso.GetComponent<Torreto>().tier == 2 && mejorarTorreta && mejorarCanon == true)
            {
                //Instancia el cañon de tier3.
                canonT3 = Instantiate(CanonT3);
                //Mira la posicion en x, y del Gameobject que se le pasa a la funcion y se los pone al prefab instaciado. 
                canonT3.transform.position = new Vector3(coso.transform.position.x, coso.transform.position.y);
                //Añade la celda del mapa en la que esta el prefab instanciado.
                posicionesOcupadas.Add(celda);
                //Añade el prefab instanciado a la lista de objetos(GameObject).
                objetos.Add(canonT3);
                //Destruye el GameObject que se le pasa a la funcion.
                Destroy(coso);
                mejorarTorreta = false;
                mejorarCatapulta = false;
            }

            if (coso.GetComponent<Torreto>().tier == 1 && mejorarTorreta && mejorarMetralleta == true)
            {
                //Instancia la metralleta de tier3.
                metralletaT2 = Instantiate(MetralletaT2);
                //Mira la posicion en x, y del Gameobject que se le pasa a la funcion y se los pone al prefab instaciado. 
                metralletaT2.transform.position = new Vector3(coso.transform.position.x, coso.transform.position.y);
                //Añade la celda del mapa en la que esta el prefab instanciado.
                posicionesOcupadas.Add(celda);
                //Añade el prefab instanciado a la lista de objetos(GameObject).
                objetos.Add(metralletaT2);
                //Destruye el GameObject que se le pasa a la funcion.
                Destroy(coso);
                mejorarTorreta = false;
                mejorarMetralleta = false;
            }
            else if (coso.GetComponent<Torreto>().tier == 2 && mejorarTorreta && mejorarMetralleta == true)
            {
                //Instancia la metralleta de tier3.
                metralletaT3 = Instantiate(MetralletaT3);
                //Mira la posicion en x, y del Gameobject que se le pasa a la funcion y se los pone al prefab instaciado. 
                metralletaT3.transform.position = new Vector3(coso.transform.position.x, coso.transform.position.y);
                //Añade la celda del mapa en la que esta el prefab instanciado.
                posicionesOcupadas.Add(celda);
                //Añade el prefab instanciado a la lista de objetos(GameObject).
                objetos.Add(metralletaT3);
                //Destruye el GameObject que se le pasa a la funcion.
                Destroy(coso);
                mejorarTorreta = false;
                mejorarMetralleta = false;
            }

            if (coso.GetComponent<Torreto>().tier == 1 && mejorarTorreta && mejorarBalista == true)
            {
                //Instancia la balista de tier3.
                balistaT2 = Instantiate(BalistaT2);
                //Mira la posicion en x, y del Gameobject que se le pasa a la funcion y se los pone al prefab instaciado. 
                balistaT2.transform.position = new Vector3(coso.transform.position.x, coso.transform.position.y);
                //Añade la celda del mapa en la que esta el prefab instanciado.
                posicionesOcupadas.Add(celda);
                //Añade el prefab instanciado a la lista de objetos(GameObject).
                objetos.Add(balistaT2);
                //Destruye el GameObject que se le pasa a la funcion.
                Destroy(coso);
                mejorarTorreta = false;
                mejorarBalista = false;
            }
            else if (coso.GetComponent<Torreto>().tier == 2 && mejorarTorreta && mejorarBalista == true)
            {
                //Instancia la balista de tier3.
                balistaT3 = Instantiate(BalistaT3);
                //Mira la posicion en x, y del Gameobject que se le pasa a la funcion y se los pone al prefab instaciado. 
                balistaT3.transform.position = new Vector3(coso.transform.position.x, coso.transform.position.y);
                //Añade la celda del mapa en la que esta el prefab instanciado.
                posicionesOcupadas.Add(celda);
                //Añade el prefab instanciado a la lista de objetos(GameObject).
                objetos.Add(balistaT3);
                //Destruye el GameObject que se le pasa a la funcion.
                Destroy(coso);
                mejorarTorreta = false;
                mejorarBalista = false;
            }
        }
    }

    //Funcion de vender una Torreta, es general, esta asignado a todos los botones vender.
    public void Vender()
    {
        if (torretoComprado == false && mejorarTorreta == false)
        {
            venderTorreta = true;
        }

    }

    //Le llegan el gameobject seleccionado y la celda en la que esta.
    private void VenderTorreta(GameObject coso, Vector3Int celda)
    {
        if (torretoComprado == false)
        {
            //Mira la variable tier de la torreta y actua en consecuencia.
            if (coso.GetComponent<Torreto>().tier == 1 && venderTorreta == true)
            {
                //Destruye el Gameobject que se le pasa a la funcion.
                Destroy(coso);
                //Como el tier es 1 se aumentan 100 a los recursos.
                NexoController.recursos += 100;
                venderTorreta = false;
            }
            else if (coso.GetComponent<Torreto>().tier == 2 && venderTorreta == true)
            {
                //Destruye el Gameobject que se le pasa a la funcion.
                Destroy(coso);
                //Como el tier es 1 se aumentan 500 a los recursos.
                NexoController.recursos += 500;
                venderTorreta = false;
            }
            else if (coso.GetComponent<Torreto>().tier == 3 && venderTorreta == true)
            {
                //Destruye el Gameobject que se le pasa a la funcion.
                Destroy(coso);
                //Como el tier es 1 se aumentan 500 a los recursos.
                NexoController.recursos += 500;
                venderTorreta = false;
            }
        }
    }
}
