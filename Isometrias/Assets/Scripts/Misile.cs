﻿using UnityEngine;

public class Misile : MonoBehaviour
{
    public float velocidad;
    public GameObject enemigo;
    public int damage;

    void FixedUpdate()
    {
        //Mover la bala hacia el enemigo al que apunta
        if (this != null && enemigo != null)
        {
            if (this.transform.position.x > enemigo.transform.position.x)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-velocidad, this.GetComponent<Rigidbody2D>().velocity.y);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(velocidad, this.GetComponent<Rigidbody2D>().velocity.y);
            }
            if (this.transform.position.y > enemigo.transform.position.y)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -velocidad);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, velocidad);
            }
        }
        else
        {
            //Prueba de redirigir bala cuando el enemigo al que apuntaba muere
            enemigo = GameObject.FindGameObjectWithTag("Enemigo");
            //Destroy(this.gameObject);
        }
    }
}
