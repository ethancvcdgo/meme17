﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NexoController : MonoBehaviour
{
    public int vida = 100;
    public static int recursos = 1000;
    public Text vidaText;
    public Text recursosText;
    // Start is called before the first frame update
    void Start()
    {
        recursosText.text = "Recursos: " + recursos;
        vidaText.text = "Vida: " + vida;
    }

    // Update is called once per frame
    void Update()
    {
        recursosText.text = "Recursos: " + recursos;
        vidaText.text = "Vida: " + vida;
        if (vida <= 0) SceneManager.LoadScene("Muerte");
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Enemigo"))
        {
            vida -= 10;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemigo"))
        {
            vida -= 10;
            Destroy(collision.gameObject);
        }
    }
}
