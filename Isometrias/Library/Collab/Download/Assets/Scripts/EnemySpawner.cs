﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour
{
    public GameObject[] blockEnemigo;
    public float ratiospawn;

    //Segundos que dura una oleada
    public float nextOleada;
    public float Speed;
    public float Vida;
    //Ver si le has dado a start para empezar la partida
    public static bool start = false;
    //Num oleadas del nivel
    public int contadorOleada;
    public Text oleadaText;
    public Button startButton;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("spawn", 1, ratiospawn);
        InvokeRepeating("chetarEnemigos", 1, 35);
    }
    public void changeState()
    {
        start = true;
        Invoke("OleadaStart", 0);
        startButton.enabled = false;
    }
    //Cada x segundos la velocidad y vida de los enemigos se mejora
    void chetarEnemigos()
    {
        if (start)
        {
            Speed = Speed * 1.035f;
            Vida = Vida * 1.35f;
        }
    }
    void OleadaStart()
    {
        oleadaText.text = "Oleadas Restantes: " + contadorOleada;
        start = true;
        Invoke("OleadaPause", nextOleada);
        contadorOleada--;
        oleadaText.text = "Oleadas Restantes: " + contadorOleada;
        if (contadorOleada<=0)
        {
            CancelInvoke();
            if (SceneManager.GetActiveScene().name.Equals("SampleScene"))
            {
                SceneManager.LoadScene("Win");
            }
            else
            {
                SceneManager.LoadScene("SampleScene");
            }
            
        }
    }
    void OleadaPause()
    {
        start = false;
        Invoke("OleadaStart", 20f);
    }
    void spawn()
    {
        //Instanciamiento aleatorio de los enemigos en la posicion del enemy spawner
        if (start)
        {
            GameObject newEnemigo = Instantiate(blockEnemigo[Random.Range(0, blockEnemigo.Length)]);
            newEnemigo.GetComponent<Pathfinding.AILerp>().speed = newEnemigo.GetComponent<Pathfinding.AILerp>().speed * Speed;
            newEnemigo.GetComponent<EnemigoVidaController>().vida = newEnemigo.GetComponent<EnemigoVidaController>().vida * Vida;
            newEnemigo.transform.position = this.transform.position;
        }
    }
}
