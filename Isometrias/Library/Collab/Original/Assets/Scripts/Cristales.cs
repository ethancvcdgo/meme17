﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cristales : MonoBehaviour
{
    public float FireRate;
    public int Recursos;
    public string tipo = "";
    public bool torreta = true;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("RecursosMasMas", 0.2f, FireRate);
    }
    private void RecursosMasMas()
    {
        //Sumas recursos a los totales
        if (EnemySpawner.start)
        {
            NexoController.recursos += Recursos;
        }
    }
}
