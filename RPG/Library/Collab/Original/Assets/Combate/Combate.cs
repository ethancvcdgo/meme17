﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Combate : MonoBehaviour
{
    // https://www.youtube.com/watch?v=u1QpCf0eCIs jrpg 1
    // https://www.youtube.com/watch?v=0QU0yV0CYT4 jrpg facil y sencillo
    // https://www.youtube.com/watch?v=iD1_JczQcFY damage popups
    // Start is called before the first frame update
    public PlayerCombat player;
    public Lobo[] lobos;
    bool turno = true;
    bool defenderse = false;
    void Start()
    {
        player.GetComponent<Animator>().SetTrigger("CombatIdle");
    }

    // Update is called once per frame
    void Update()
    {
        if (player.HP<=0)
        {
            Debug.Log("Big F");
            SceneManager.LoadScene("MainMenu");
        }
        if (!turno)
        {
            bool muertos = true;
            foreach (Lobo lob in lobos)
            {
                if (lob != null)
                {
                    muertos = false;
                    int r = Random.Range(0, 2);
                    switch (r)
                    {
                        case 0:
                            //"ataque"
                            lob.GetComponent<Transform>().position = new Vector2(42.35f, lob.transform.position.y);
                            int dano = lob.DAMAGE - player.ARMOR;
                            if (dano<=0)
                            {
                                dano = 1;
                            }
                            if (defenderse)
                            {
                                dano /= 2;
                            }
                            player.HP -= dano;
                            Debug.Log("El enemigo te ataca, te hace: "+dano);
                            Invoke("volverEnemigo",2);
                            break;
                        case 1:
                            Debug.Log("El enemigo hace cosas");
                            break;
                        default:
                            Debug.Log("El enemigo se te queda mirando");
                            break;
                    }
                }
            }
            if (defenderse)
            {
                defenderse = false;
            }
            if (muertos)
            {
                Debug.Log("Ha muerto lo morible");
                Player.HP = player.HP;
                Player.MANA = player.MP;
                Player.OROS += 10;
                Player.EXP += 100;
                if (Player.EXP>=Player.MAXEXP)
                {
                    Player.EXP = Player.EXP % Player.MAXEXP;
                    if (Player.LVL<3)
                    {
                        Player.LVL++;
                    }
                }
                Player.lobos = true;
                SceneManager.LoadScene("SceneWolves");
            }
            turno = true;
        }
    }

    private void volver()
    {
        player.GetComponent<Transform>().position = new Vector2(41.35f, player.transform.position.y);
        turno = false;
    }
    private void volverEnemigo()
    {
        foreach (Lobo lob in lobos)
        {
            if (lob != null)
            {
                lob.GetComponent<Transform>().position = new Vector2(47.21f, lob.transform.position.y);
            }
        }
        turno = true;
    }

    public void atqNorm()
    {
        if (turno)
        {
            player.GetComponent<Animator>().SetTrigger("AtaqueMelee");
            player.GetComponent<Transform>().position = new Vector2(46.22f, player.transform.position.y);
            if (lobos.Length > 0)
            {
                int num=0;
                foreach (Lobo lob in lobos)
                {
                    if (lob != null)
                    {
                        int dano = player.DAMAGE - lob.ARMOR;
                        if (dano <= 0)
                        {
                            dano = 1;
                        }
                        lob.HP -= dano;
                        if (lob.HP <= 0)
                        {
                            Debug.Log("Enemigo muerto");
                            Destroy(lob.gameObject);
                            lobos[num] = null;
                        }
                        break;
                    }
                    num++;
                }
            }
            Invoke("volver", 2);
        }
    }
    public void defensa()
    {
        if (turno)
        {
            Debug.Log("Te defiendes");
            defenderse = true;
            turno = !turno;
        }
    }
    public void hab1AtArea()
    {
        if (turno)
        {
            if (player.MP >= 1)
            {
                player.GetComponent<Animator>().SetTrigger("AtaqueMelee");
                player.GetComponent<Transform>().position = new Vector2(46.22f, player.transform.position.y);
                if (lobos.Length > 0)
                {
                    int num = 0;
                    foreach (Lobo lob in lobos)
                    {
                        if (lob != null)
                        {
                            int dano = player.DAMAGE - lob.ARMOR;
                            if (dano <= 0)
                            {
                                dano = 1;
                            }
                            lob.HP -= dano;
                            if (lob.HP <= 0)
                            {
                                Debug.Log("Enemigo muerto");
                                Destroy(lob.gameObject);
                                lobos[num] = null;
                            }
                        }
                        num++;
                    }
                    player.MP--;
                }
                Invoke("volver", 2);
            }
        }
    }
    public void hab2AtFuerte()
    {
        if (turno)
        {
            if (player.MP >= 1)
            {
                player.GetComponent<Animator>().SetTrigger("AtaqueMelee");
                player.GetComponent<Transform>().position = new Vector2(46.22f, player.transform.position.y);
                if (lobos.Length > 0)
                {
                    int num = 0;
                    foreach (Lobo lob in lobos)
                    {
                        if (lob != null)
                        {
                            int dano = player.DAMAGE - lob.ARMOR;
                            if (dano <= 0)
                            {
                                dano = 1;
                            }
                            lob.HP -= dano * 2;
                            if (lob.HP <= 0)
                            {
                                Debug.Log("Enemigo muerto");
                                Destroy(lob.gameObject);
                                lobos[num] = null;
                            }
                            break;
                        }
                        num++;
                    }
                    player.MP--;
                }
                Invoke("volver", 2);
            }
        }
    }
    public void magiaMierder()
    {
        if (turno)
        {
            if (player.MP >= 2)
            {
                player.GetComponent<Animator>().SetTrigger("AtaqueMelee");
                player.GetComponent<Transform>().position = new Vector2(46.22f, player.transform.position.y);
                if (lobos.Length > 0)
                {
                    int num = 0;
                    foreach (Lobo lob in lobos)
                    {
                        if (lob != null)
                        {
                            int dano = player.DAMAGE+(player.MP/2);
                            if (dano <= 0)
                            {
                                dano = 1;
                            }
                            lob.HP -= dano;
                            if (lob.HP <= 0)
                            {
                                Debug.Log("Enemigo muerto");
                                Destroy(lob.gameObject);
                                lobos[num] = null;
                            }
                            break;
                        }
                        num++;
                    }
                    player.MP-=2;
                }
                Invoke("volver", 2);
            }
        }
    }
    public void Huida()
    {
        if (turno)
        {
            int num = Random.Range(0, 10);
            if (num == 3||num==5)
            {
                Debug.Log("Has huido cual Francés");
                Player.HP = player.HP;
                Player.MANA = player.MP;
                SceneManager.LoadScene("SceneWolves");
            }
            else
            {
                Debug.Log("No has logrado huir, Francés");
                turno = !turno;
            }
        }
    }
}
