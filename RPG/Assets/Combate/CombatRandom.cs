﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CombatRandom : MonoBehaviour
{
    //no comento esto porque es practicamente igual al de Combate.cs
    public PlayerCombat player;
    public Lobo[] lobos;
    bool turno = true;
    bool defenderse = false;
    void Start()
    {
        player.GetComponent<Animator>().SetTrigger("CombatIdle");
        Debug.Log("idlebase");
    }

    // Update is called once per frame
    void Update()
    {
        if (player.HP <= 0)
        {
            Debug.Log("Big F");
            SceneManager.LoadScene("MainMenu");
        }
        if (!turno)
        {
            bool muertos = true;
            foreach (Lobo lob in lobos)
            {
                if (lob != null)
                {
                    muertos = false;
                    int r = Random.Range(0, 2);
                    switch (r)
                    {
                        case 0:
                            //"ataque"
                            lob.GetComponent<Transform>().position = new Vector2(42.35f, lob.transform.position.y);
                            int dano = lob.DAMAGE - player.ARMOR;
                            if (dano <= 0)
                            {
                                dano = 1;
                            }
                            if (defenderse)
                            {
                                dano /= 2;
                            }
                            player.HP -= dano;
                            Debug.Log("El enemigo te ataca, te hace: " + dano);
                            break;
                        case 1:
                            Debug.Log("El enemigo hace cosas");
                            break;
                        default:
                            Debug.Log("El enemigo se te queda mirando");
                            break;
                    }
                    Invoke("volverEnemigo", 2);
                }
            }
            if (defenderse)
            {
                defenderse = false;
            }
            if (muertos)
            {
                Debug.Log("Ha muerto lo morible");
                Player.HP = player.HP;
                Player.MANA = player.MP;
                Player.OROS += 5;
                Player.EXP += 10;
                if (Player.EXP >= Player.MAXEXP)
                {
                    Player.EXP = Player.EXP % Player.MAXEXP;
                    if (Player.LVL < 3)
                    {
                        Player.LVL++;
                        Player.HP = Player.MAXHP;
                        Player.MANA = Player.MAXMANA;
                    }
                }
                Player.lobos = true;
                SceneManager.LoadScene("SampleScene");
            }
            turno = true;
        }
    }

    private void volver()
    {
        player.GetComponent<Transform>().position = new Vector2(41.35f, player.transform.position.y);
        turno = false;
    }
    private void volverEnemigo()
    {
        foreach (Lobo lob in lobos)
        {
            if (lob != null)
            {
                lob.GetComponent<Transform>().position = new Vector2(47.21f, lob.transform.position.y);
            }
        }
        turno = true;
    }

    public void atqNorm()
    {
        if (turno)
        {
            player.GetComponent<Animator>().SetTrigger("AtaqueMelee");
            Debug.Log("ataquebase");
            player.GetComponent<Transform>().position = new Vector2(45.22f, player.transform.position.y);
            if (lobos.Length > 0)
            {
                int num = 0;
                foreach (Lobo lob in lobos)
                {
                    if (lob != null)
                    {
                        int dano = player.DAMAGE - lob.ARMOR;
                        if (dano <= 0)
                        {
                            dano = 1;
                        }
                        lob.HP -= dano;
                        if (lob.HP <= 0)
                        {
                            Debug.Log("Enemigo muerto");
                            Destroy(lob.gameObject);
                            lobos[num] = null;
                        }
                        break;
                    }
                    num++;
                }
            }
            Invoke("volver", 2);
            turno = false;
        }
    }
    public void defensa()
    {
        if (turno)
        {
            Debug.Log("Te defiendes");
            defenderse = true;
            turno = false;
        }
    }
    public void hab1AtArea()
    {
        if (turno)
        {
            if (player.MP >= 1)
            {
                player.GetComponent<Animator>().SetTrigger("AtaqueMelee");
                Debug.Log("ataquearea");
                player.GetComponent<Transform>().position = new Vector2(45.22f, player.transform.position.y);
                if (lobos.Length > 0)
                {
                    int num = 0;
                    foreach (Lobo lob in lobos)
                    {
                        if (lob != null)
                        {
                            int dano = player.DAMAGE - lob.ARMOR;
                            if (dano <= 0)
                            {
                                dano = 1;
                            }
                            lob.HP -= dano;
                            if (lob.HP <= 0)
                            {
                                Debug.Log("Enemigo muerto");
                                Destroy(lob.gameObject);
                                lobos[num] = null;
                            }
                        }
                        num++;
                    }
                    player.MP--;
                }
                Invoke("volver", 2);
                turno = false;
            }
        }
    }
    public void hab2AtFuerte()
    {
        if (turno)
        {
            if (player.MP >= 1)
            {
                player.GetComponent<Animator>().SetTrigger("AtaqueMelee");
                Debug.Log("smite");
                player.GetComponent<Transform>().position = new Vector2(45.22f, player.transform.position.y);
                if (lobos.Length > 0)
                {
                    int num = 0;
                    foreach (Lobo lob in lobos)
                    {
                        if (lob != null)
                        {
                            int dano = player.DAMAGE - lob.ARMOR;
                            if (dano <= 0)
                            {
                                dano = 1;
                            }
                            lob.HP -= dano * 2;
                            if (lob.HP <= 0)
                            {
                                Debug.Log("Enemigo muerto");
                                Destroy(lob.gameObject);
                                lobos[num] = null;
                            }
                            break;
                        }
                        num++;
                    }
                    player.MP--;
                }
                Invoke("volver", 2);
                turno = false;
            }
        }
    }
    public void magiaMierder()
    {
        if (turno)
        {
            if (player.MP >= 2)
            {
                player.GetComponent<Animator>().SetTrigger("AtaqueMelee");
                Debug.Log("magia");
                player.GetComponent<Transform>().position = new Vector2(45.22f, player.transform.position.y);
                if (lobos.Length > 0)
                {
                    int num = 0;
                    foreach (Lobo lob in lobos)
                    {
                        if (lob != null)
                        {
                            int dano = player.DAMAGE + (player.MP / 2);
                            if (dano <= 0)
                            {
                                dano = 1;
                            }
                            lob.HP -= dano;
                            if (lob.HP <= 0)
                            {
                                Debug.Log("Enemigo muerto");
                                Destroy(lob.gameObject);
                                lobos[num] = null;
                            }
                            break;
                        }
                        num++;
                    }
                    player.MP -= 2;
                }
                Invoke("volver", 2);
                turno = false;
            }
        }
    }
    public void Huida()
    {
        if (turno)
        {
            int num = Random.Range(0, 10);
            if (num == 3 || num == 5)
            {
                Debug.Log("Has huido cual Francés");
                Player.HP = player.HP;
                Player.MANA = player.MP;
                SceneManager.LoadScene("SampleScene");
            }
            else
            {
                Debug.Log("No has logrado huir, Francés");
                turno = !turno;
            }
        }
    }
    public void fueraTurno()
    {
        if (turno)
        {
            turno = false;
        }
    }
}
