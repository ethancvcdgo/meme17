﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Combate : MonoBehaviour
{
    // Start is called before the first frame update
    public PlayerCombat player;
    public Lobo[] lobos;
    bool turno = true;
    bool defenderse = false;
    void Start()
    {
        player.GetComponent<Animator>().SetTrigger("CombatIdle");
    }

    // Update is called once per frame
    void Update()
    {
        //
        if (player.HP<=0)
        {
            Debug.Log("Big F");
            SceneManager.LoadScene("MainMenu");
        }
        if (!turno)
        {
            bool muertos = true;
            foreach (Lobo lob in lobos)
            {
                if (lob != null)
                {
                    muertos = false;
                    int r = Random.Range(0, 2);
                    switch (r)
                    {
                        case 0:
                            //"ataque"
                            lob.GetComponent<Transform>().position = new Vector2(42.35f, lob.transform.position.y);
                            int dano = lob.DAMAGE - player.ARMOR;
                            if (dano<=0)
                            {
                                dano = 1;
                            }
                            if (defenderse)
                            {
                                dano /= 2;
                            }
                            player.HP -= dano;
                            Debug.Log("El enemigo te ataca, te hace: "+dano);
                            break;
                        case 1:
                            Debug.Log("El enemigo hace cosas");
                            break;
                        default:
                            Debug.Log("El enemigo se te queda mirando");
                            break;
                    }
                    Invoke("volverEnemigo", 2);
                }
            }
            if (defenderse)
            {
                defenderse = false;
            }
            if (muertos)
            {
                //cosas que te dan al matar todos los enemigos del combate
                Debug.Log("Ha muerto lo morible");
                Player.HP = player.HP;
                Player.MANA = player.MP;
                Player.OROS += 10;
                Player.EXP += 100;
                if (Player.EXP>=Player.MAXEXP)
                {
                    Player.EXP = Player.EXP % Player.MAXEXP;
                    if (Player.LVL<3)
                    {
                        Player.LVL++;
                        Player.HP = Player.MAXHP;
                        Player.MANA = Player.MAXMANA;
                    }
                }
                Player.lobos = true;
                SceneManager.LoadScene("SceneWolves");
            }
            turno = true;
        }
    }
    //volver a la posicion inicial
    private void volver()
    {
        player.GetComponent<Transform>().position = new Vector2(41.35f, player.transform.position.y);
        turno = false;
    }
    //same as player one
    private void volverEnemigo()
    {
        foreach (Lobo lob in lobos)
        {
            if (lob != null)
            {
                lob.GetComponent<Transform>().position = new Vector2(47.21f, lob.transform.position.y);
            }
        }
        turno = true;
    }
    //ataque a un enemego(primero de la lista)
    public void atqNorm()
    {
        if (turno)
        {
            player.GetComponent<Animator>().SetTrigger("AtaqueMelee");
            player.GetComponent<Transform>().position = new Vector2(46.22f, player.transform.position.y);
            if (lobos.Length > 0)
            {
                int num=0;
                foreach (Lobo lob in lobos)
                {
                    if (lob != null)
                    {
                        int dano = player.DAMAGE - lob.ARMOR;
                        if (dano <= 0)
                        {
                            dano = 1;
                        }
                        lob.HP -= dano;
                        if (lob.HP <= 0)
                        {
                            Debug.Log("Enemigo muerto");
                            Destroy(lob.gameObject);
                            lobos[num] = null;
                        }
                        break;
                    }
                    num++;
                }
            }
            Invoke("volver", 2);
            turno = false;
        }
    }
    //activa el booleano para recibir mitad de daño en el turno de los enemigos
    public void defensa()
    {
        if (turno)
        {
            Debug.Log("Te defiendes");
            defenderse = true;
            turno = false;
        }
    }
    //ataque a todos lo enemigos
    public void hab1AtArea()
    {
        if (turno)
        {
            if (player.MP >= 1)
            {
                player.GetComponent<Animator>().SetTrigger("AtaqueMelee");
                player.GetComponent<Transform>().position = new Vector2(46.22f, player.transform.position.y);
                if (lobos.Length > 0)
                {
                    int num = 0;
                    foreach (Lobo lob in lobos)
                    {
                        if (lob != null)
                        {
                            int dano = player.DAMAGE - lob.ARMOR;
                            if (dano <= 0)
                            {
                                dano = 1;
                            }
                            lob.HP -= dano;
                            if (lob.HP <= 0)
                            {
                                Debug.Log("Enemigo muerto");
                                Destroy(lob.gameObject);
                                lobos[num] = null;
                            }
                        }
                        num++;
                    }
                    player.MP--;
                }
                Invoke("volver", 2);
                turno = false;
            }
        }
    }
    //ataque mas fuerte
    public void hab2AtFuerte()
    {
        if (turno)
        {
            if (player.MP >= 1)
            {
                player.GetComponent<Animator>().SetTrigger("AtaqueMelee");
                player.GetComponent<Transform>().position = new Vector2(46.22f, player.transform.position.y);
                if (lobos.Length > 0)
                {
                    int num = 0;
                    foreach (Lobo lob in lobos)
                    {
                        if (lob != null)
                        {
                            int dano = player.DAMAGE - lob.ARMOR;
                            if (dano <= 0)
                            {
                                dano = 1;
                            }
                            lob.HP -= dano * 2;
                            if (lob.HP <= 0)
                            {
                                Debug.Log("Enemigo muerto");
                                Destroy(lob.gameObject);
                                lobos[num] = null;
                            }
                            break;
                        }
                        num++;
                    }
                    player.MP--;
                }
                Invoke("volver", 2);
                turno = false;
            }
        }
    }
    //ataque magico que hace daño verdadero dependiendo del mana actual
    public void magiaMierder()
    {
        if (turno)
        {
            if (player.MP >= 2)
            {
                player.GetComponent<Animator>().SetTrigger("AtaqueMelee");
                player.GetComponent<Transform>().position = new Vector2(46.22f, player.transform.position.y);
                if (lobos.Length > 0)
                {
                    int num = 0;
                    foreach (Lobo lob in lobos)
                    {
                        if (lob != null)
                        {
                            int dano = player.DAMAGE+(player.MP/2);
                            if (dano <= 0)
                            {
                                dano = 1;
                            }
                            lob.HP -= dano;
                            if (lob.HP <= 0)
                            {
                                Debug.Log("Enemigo muerto");
                                Destroy(lob.gameObject);
                                lobos[num] = null;
                            }
                            break;
                        }
                        num++;
                    }
                    player.MP-=2;
                }
                Invoke("volver", 2);
                turno = false;
            }
        }
    }
    //huyes del combate(a veces)
    public void Huida()
    {
        if (turno)
        {
            int num = Random.Range(0, 10);
            if (num == 3||num==5)
            {
                Debug.Log("Has huido cual Francés");
                Player.HP = player.HP;
                Player.MANA = player.MP;
                SceneManager.LoadScene("SceneWolves");
            }
            else
            {
                Debug.Log("No has logrado huir, Francés");
                turno = !turno;
            }
        }
    }
    //cosa puesta para que los objetos te quiten el turno al usarlos
    public void fueraTurno()
    {
        if (turno)
        {
            turno = false;
        }
    }
}
