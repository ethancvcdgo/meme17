﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCombat : MonoBehaviour
{
    public int MAXHP;
    public int HP;
    public int MAXMANA;
    public int MP;
    public int DAMAGE;
    public int ARMOR;
    public int LVL;
    public int EXP;
    public Text TextXP;
    public Text TextLVL;
    public Text TextHP;
    public Text TextMP;

    // Start is called before the first frame update
    void Start()
    {
        HP = Player.HP;
        MAXHP = Player.MAXHP;
        MP = Player.MANA;
        MAXMANA = Player.MAXMANA;
        DAMAGE = Player.DAMAGE;
        ARMOR = Player.ARMOR;
        LVL = Player.LVL;
        EXP = Player.EXP;
        
    }

    // Update is called once per frame
    void Update()
    {
        TextXP.text = EXP + "";
        TextLVL.text = LVL + "";
        TextHP.text = HP + " / " + MAXHP;
        TextMP.text = MP + " / " + MAXMANA;
    }


}
