﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CombatMenu : MonoBehaviour
{

    public List<Item> listaConsumables = new List<Item>();

    public static bool Paused = false;
    public GameObject AtaquesMenuUI;
    public GameObject ConsumablesUI;
    public GameObject StatsMenuUI;


    private void Start()
    {
        updatePanelSlots();
    }

    void Update()
    {
        updatePanelSlots();
    }

    //activar/desactivar menus
    public void AtaquesMenu()
    {
        AtaquesMenuUI.SetActive(true);
        ConsumablesUI.SetActive(false);
    }

    //activar/desactivar menus
    public void ObjetosMenu()
    {
        ConsumablesUI.SetActive(true);
        AtaquesMenuUI.SetActive(false);

    }

    public void updatePanelSlots()
    {
        
        int i = 0;
        //foreach loop to iterate through the panel's children.
        foreach (Transform child in ConsumablesUI.transform)
        {
            inventorySlotControllerCombat PanelObjeto = child.GetComponent<inventorySlotControllerCombat>();

            if (i < Inventario.instance.listaConsumables.Count)
            {
                PanelObjeto.item = Inventario.instance.listaConsumables[i];
            }
            else
            {
                PanelObjeto.item = null;
            }
            PanelObjeto.updateInfo();
            i++;
        }

    }
}