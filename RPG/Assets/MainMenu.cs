﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public static bool Paused = false;
    public GameObject MainMenuUI;
    public Player p;

    private void Awake()
    {
        //te borra el player si llegas a este escena desde ingame
        if(Player.player != null)
        Player.Destroy(Player.player.gameObject);

    }

    //empezar partida nueva
    public void startGame()
    {
        //setea la vida y el mana para no empezar a 0
        Player.HP = Player.MAXHP;
        Player.MANA = Player.MAXMANA;
        SceneManager.LoadScene("SceneWolves");
    }
    //cargar partida
    public void loadGame()
    {
        p.LoadGame();
        
    }
}
