﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public Animator animator;
    //valores del personaje
    public int vel;
    // estos 4 para la camara para que no se salga por los lados en el overworld
    private bool bordesDerecha = false;
    private bool bordesIzquierda = false;
    private bool bordesInferior = false;
    private bool bordesSuperior = false;
    static bool cargado = false;
    private static Vector2 scenecargada;
    public static bool ciudad = false;
    public static bool lobos = false;
    public static int MAXHP;
    public static int HP;
    public static int MAXMANA;
    public static int MANA;
    public static int DAMAGE;
    public static int ARMOR;
    public static int LVL;
    public static int EXP;
    public static int MAXEXP=100;
    public static int OROS;
    public Camera gameCamera;
    public static GameObject player = null;

    void Awake()
    {
        vel = 5;
        //si entras a lobos despues de haber hecho el combate te borra el combate
        if (SceneManager.GetActiveScene().name.Equals("SceneWolves"))
        {
            if (lobos)
            {
                Destroy(GameObject.Find("Lobo1"));
                Destroy(GameObject.Find("Lobo2"));
                Destroy(GameObject.Find("CombateLobos"));
            }
        }
        //cosas del singleton
        if (player == null)
        {
            player = this.gameObject;
            DontDestroyOnLoad(player);
        }
        else
        {
            Destroy(this.gameObject);
        }
        //setear camara para que no muera
        gameCamera = Camera.main;


    }
    // Start is called before the first frame update
    void Start()
    {
        if (SceneManager.GetActiveScene().name.Equals("SceneWolves"))
        {
            Inventario.lastscene = SceneManager.GetActiveScene().name;
            SceneManager.LoadScene("Pruebas de inventario");
        }
        //setear vidamax y mana dependiendo del level
        if (LVL==0)
        {
            MAXHP = 14;
            MAXMANA = 10;
            LVL = 1;
            EXP = 0;
            HP = MAXHP;
            MANA = MAXMANA;
        }
        else if (LVL == 1)
        {
            MAXHP = 14;
            MAXMANA = 10;
            HP = MAXHP;
            MANA = MAXMANA;
        }
        else if (LVL == 2)
        {
            MAXHP = 24;
            MAXMANA = 15;
        }
        else if (LVL == 3)
        {
            MAXHP = 34;
            MAXMANA = 20;
        }
        if (cargado)
        {
            this.transform.position = scenecargada;
            cargado = false;
        }
        Inventario.lastscene = SceneManager.GetActiveScene().name;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(ARMOR);
        //Debug.Log(DAMAGE);

        if (gameCamera == null)
        {
            gameCamera = Camera.main;
            bordesDerecha = false;
            bordesIzquierda = false;
            bordesInferior = false;
            bordesSuperior = false;
        }
        if (this != null)
        {
            //porque si no se queda a 0(lo mas inteligente que he hecho en este juego)
            try
            {
                DAMAGE = ((Equipable)Inventario.instance.listaEquipables[0]).damage;
                ARMOR = ((Equipable)Inventario.instance.listaEquipables[1]).armor;
            }
            catch (System.Exception)
            {
                Inventario.lastscene = SceneManager.GetActiveScene().name;
                SceneManager.LoadScene("Pruebas de inventario");
            }
            
            if (SceneManager.GetActiveScene().name.Equals("SampleScene"))
            {
                if (Random.Range(0, 1435) == 35)
                {
                    Debug.Log("Aqui se genera un combate aleatorio");
                    SceneManager.LoadScene("CombatRandom");
                    this.transform.position = new Vector3(
                       44.48f,
                       4.46f,
                       this.transform.position.z);
                }
            }
            //borra personaje no deseado en combate
            if (SceneManager.GetActiveScene().name.Equals("CombatRandom"))
            {
                Destroy(this.gameObject);
            }
            if (SceneManager.GetActiveScene().name.Equals("Combat"))
            {
                Destroy(this.gameObject);
            }
            if (SceneManager.GetActiveScene().name.Equals("CombatBoss"))
            {
                Destroy(this.gameObject);
            }
            //cosas de la camara pa que no se salga del mapa
            if ((bordesInferior && bordesDerecha) || (bordesInferior && bordesIzquierda) || (bordesSuperior && bordesIzquierda))
            {
                gameCamera.transform.position = new Vector3(
               gameCamera.transform.position.x,
               gameCamera.transform.position.y,
               gameCamera.transform.position.z);
            }
            else if (bordesSuperior)
            {
                gameCamera.transform.position = new Vector3(
                this.transform.position.x,
                gameCamera.transform.position.y,
                gameCamera.transform.position.z);
            }
            else if (bordesIzquierda)
            {
                gameCamera.transform.position = new Vector3(
                gameCamera.transform.position.x,
                this.transform.position.y,
                gameCamera.transform.position.z);
            }
            else if (bordesInferior)
            {
                gameCamera.transform.position = new Vector3(
               this.transform.position.x,
               gameCamera.transform.position.y,
               gameCamera.transform.position.z);
            }
            else if (bordesDerecha)
            {
                gameCamera.transform.position = new Vector3(
                 gameCamera.transform.position.x,
                 this.transform.position.y,
                 gameCamera.transform.position.z);
            }
            else
            {
                gameCamera.transform.position = new Vector3(
                   this.transform.position.x,
                   this.transform.position.y,
                   gameCamera.transform.position.z);
            }
            //inputs pa moverte
            if (Input.GetKey("w"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, vel);
                animator.SetBool("boolUp", true);
            }
            else if (Input.GetKey("s"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
                animator.SetBool("boolDown", true);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
                animator.SetBool("boolDown", false);
                animator.SetBool("boolUp", false);
            }

            if (Input.GetKey("d"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
                animator.SetBool("boolRight", true);
                transform.localScale = new Vector3(1f, 1f, 1f);
            }
            else if (Input.GetKey("a"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
                animator.SetBool("boolRight", true);
                transform.localScale = new Vector3(-1f, 1f, 1f);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
                animator.SetBool("boolRight", false);
            }
            //abrir inventario
            if (Input.GetKey("i"))
            {
                if (SceneManager.GetActiveScene().name != "Pruebas de inventario") {
                    Inventario.lastscene = SceneManager.GetActiveScene().name;
                    SceneManager.LoadScene("Pruebas de inventario");
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "BordeDerecho")
        {
            bordesDerecha = true;
        }
        if (collision.gameObject.name == "BordeIzquierdo")
        {
            bordesIzquierda = true;
        }
        if (collision.gameObject.name == "BordeInferior")
        {
            bordesInferior = true;
        }
        if (collision.gameObject.name == "BordeSuperior")
        {
            bordesSuperior = true;
        }
        if (collision.gameObject.name == "Pueblo")
        {
            ciudad = true;
        }
        if (collision.gameObject.name == "Piedras")
        {
            SceneManager.LoadScene("SceneWolves");
            this.transform.position = new Vector3(
                40,
                8,
                this.transform.position.z);
        }
        if (collision.gameObject.name == "SalidaLobos")
        {
            SceneManager.LoadScene("SampleScene");
            this.transform.position = new Vector3(
                -11,
                -3,
                this.transform.position.z);
            bordesIzquierda = true;
        }
        if (collision.gameObject.name == "CombateLobos")
        {
            SceneManager.LoadScene("Combat");
            this.transform.position = new Vector3(
                44.48f,
                4.46f,
                this.transform.position.z);
        }
        if (collision.gameObject.name == "GottaGoCueva")
        {
            this.transform.position = new Vector3(
                -15.89f,
                18.4f,
                this.transform.position.z);
        }
        if (collision.gameObject.name == "BossBat")
        {
            SceneManager.LoadScene("CombatBoss");
            this.transform.position = new Vector3(
                44.48f,
                4.46f,
                this.transform.position.z);
        }

        if (collision.gameObject.name == "PuertaAbajoSalida")
        {
            this.transform.position = new Vector3(
                64,
                2,
                this.transform.position.z);
        }
        if (collision.gameObject.name == "PuertaArribaSalida")
        {
            this.transform.position = new Vector3(
                67,
                8,
                this.transform.position.z);
        }
        if (collision.gameObject.name == "PuertaHab1Salida")
        {
            this.transform.position = new Vector3(
                74,
                21,
                this.transform.position.z);
        }
        if (collision.gameObject.name == "PuertaHab2Salida")
        {
            this.transform.position = new Vector3(
                78,
                21,
                this.transform.position.z);
        }
        if (collision.gameObject.name == "PuertaHab3Salida")
        {
            this.transform.position = new Vector3(
                85,
                21,
                this.transform.position.z);
        }
        if (collision.gameObject.name == "PuertaSalidaSalida")
        {
            SceneManager.LoadScene("SampleScene");
            this.transform.position = new Vector3(
                12,
                -6,
                this.transform.position.z);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.name == "PuertaAbajoEntrada")
        {
            if (Input.GetKeyDown("f"))
            {
                this.transform.position = new Vector3(
                96,
                -3,
                this.transform.position.z);
            }
        }
        if (collision.gameObject.name == "PuertaArribaEntrada")
        {
            if (Input.GetKeyDown("f"))
            {
                this.transform.position = new Vector3(
                82,
                19,
                this.transform.position.z);
            }
        }
        if (collision.gameObject.name == "PuertaHab1Entrada")
        {
            if (Input.GetKeyDown("f"))
            {
                this.transform.position = new Vector3(
                74,
                31,
                this.transform.position.z);
            }
        }
        if (collision.gameObject.name == "PuertaHab2Entrada")
        {
            if (Input.GetKeyDown("f"))
            {
                this.transform.position = new Vector3(
                82,
                31,
                this.transform.position.z);
            }
        }
        if (collision.gameObject.name == "PuertaHab3Entrada")
        {
            if (Input.GetKeyDown("f"))
            {
                this.transform.position = new Vector3(
                93,
                31,
                this.transform.position.z);
            }
        }
        if (collision.gameObject.name == "Piano")
        {
            if (Input.GetKeyDown("f"))
            {
                //poner cosas pal piano
                musica.piano = true;
            }
        }
        if (collision.gameObject.name == "Piano2")
        {
            Debug.Log("owo");
            if (Input.GetKeyDown("f"))
            {
                //poner cosas pal piano
                musica.piano = true;
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name == "BordeDerecho")
        {
            bordesDerecha = false;
        }
        if (collision.gameObject.name == "BordeIzquierdo")
        {
            bordesIzquierda = false;
        }
        if (collision.gameObject.name == "BordeInferior")
        {
            bordesInferior = false;
        }
        if (collision.gameObject.name == "BordeSuperior")
        {
            bordesSuperior = false;
        }
    }
    //esto al final no esta aqui, esta en pause menu
    /*private Save createSave()
    {
        Save save = new Save();
        save.Dinero = OROS;
        save.HP = HP;
        save.MP = MANA;
        save.Scene = SceneManager.GetActiveScene().name;
        save.xC = this.transform.position.x;
        save.yC = this.transform.position.y;
        save.LVL = LVL;
        save.XP = EXP;
        return save;

    }*/
    /*public void saveGame()
    {
        Debug.Log(Application.persistentDataPath + "/gamesave.save");
        Save save = this.createSave();
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
        bf.Serialize(file, save);
        file.Close();
        Debug.Log("Game Saved");
    }*/

        //cargar partida
    public void LoadGame()
    {
        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
            Save save = (Save)bf.Deserialize(file);
            SceneManager.LoadScene(save.Scene);
            OROS =save.Dinero;
            HP = save.HP;
            MANA = save.MP;
            LVL = save.LVL;
            EXP = save.XP;
            lobos = save.lobos;
            Debug.Log("Game Loaded");
            file.Close();
            scenecargada = new Vector2(save.xC, save.yC);
            cargado = true;
        }
        else
        {
            Debug.Log("No game saved!");
        }
    }
}



