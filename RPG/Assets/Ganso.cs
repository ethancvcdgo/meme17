﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ganso : MonoBehaviour
{
    //este script hace que el ganso camine hacia un lado, se choque con algo y cambie de direccion
    private bool derecha = true;
    public int vel = 5;
    //this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
    //this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);

    // Update is called once per frame
    void Update()
    {
        if (derecha)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
            transform.localScale = new Vector3(1f, 1f, 1f);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
            transform.localScale = new Vector3(-1f, 1f, 1f);
        }
        
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        derecha = !derecha;
        
    }

}
