﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Save
{
    public string Scene;
    public float xC;
    public float yC;
    public int LVL;
    public int XP;
    public int Dinero;
    public int HP;
    public int MP;
    public bool lobos;
}
