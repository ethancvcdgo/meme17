﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//con esto te crea un menu para hacer equipables a lo click derecho crear y salen items y tal
[CreateAssetMenu(fileName = "new Equipable", menuName = "Items/Equipable")]
public class Equipable : Item
{
    //atributos de los equipables
    public bool equipado;
    public int armor;
    public int damage;
}
