﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//con esto te crea un menu para hacer consumables a lo click derecho crear y salen items y tal
[CreateAssetMenu (fileName = "new Consumable", menuName = "Items/Consumable")]
public class Consumable : Item
{
    //atributos de los consumables
    public int heal = 0;
    public int manaheal = 0;
    public Consumable() : base()
    {

    }
    //constructor
    public Consumable(string itemName, string itemQuantity, string itemDescription, Sprite itemIcon, int heal, int manaheal) : base(itemName, itemQuantity, itemDescription, itemIcon)
    {
        this.heal = heal;
        this.manaheal = manaheal;
    }

    //funcion de usar consumables desde el menu, si estas en el cap de vida/mana no puedes curarte vida/mana
    public override void Use()
    {
        if (Player.HP < Player.MAXHP)
        {
            if (Player.HP + heal > Player.MAXHP)
            {
                Player.HP = Player.MAXHP;
            }
            else
            {
                Player.HP = Player.HP + heal;
            }
            Inventario.instance.Remove(this);
        }
        if (Player.MANA < Player.MAXMANA)
        {
            if (Player.MANA + manaheal > Player.MAXMANA)
            {
                Player.MANA = Player.MAXMANA;
            }
            else
            {
                Player.MANA = Player.MANA + manaheal;
            }
            Inventario.instance.Remove(this);
        }
    }

    //funcion de usar consumables desde el menu de combate, si estas en el cap de vida/mana no puedes curarte vida/mana, en este caso solo en las escenas de combate
    public override void UseCombat(PlayerCombat player)
    {
        if (player.HP < player.MAXHP)
        {
            if (player.HP + heal > player.MAXHP)
            {
                player.HP = player.MAXHP;
            }
            else
            {
                player.HP = player.HP + heal;
            }
            Inventario.instance.Remove(this);
        }
        if (player.MP < player.MAXMANA)
        {
            if (player.MP + manaheal > player.MAXMANA)
            {
                player.MP = player.MAXMANA;
            }
            else
            {
                player.MP = player.MP + manaheal;
            }
            Inventario.instance.Remove(this);
        }

    } 
}
