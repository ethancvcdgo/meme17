﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Item : ScriptableObject
{
    //se declaran los atributos del objeto
    public string itemName;
    public Sprite itemIcon;
    public string itemQuantity;
    public string itemDescription;
    public Item()
    {

    }
    // constructor de los objetos
    public Item(string itemName, string itemQuantity, string itemDescription, Sprite itemIcon)
    {
        this.itemName = itemName;
        this.itemQuantity = itemQuantity;
        this.itemDescription = itemDescription;
        this.itemIcon = itemIcon;
    }
    // This function uses the keyword virtual this allows each Item type to overwrite this function.
    public virtual void Use()
    {

    }
    // This function uses the keyword virtual this allows each Item type to overwrite this function.
    public virtual void UseCombat(PlayerCombat player)
    {

    }
}
