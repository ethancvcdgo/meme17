﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Inventario : MonoBehaviour
{
    public static String lastscene;
    private bool inicial = true;
    //lista de Items
    public List<Item> listaConsumables = new List<Item>();
    public List<Item> listaEquipables = new List<Item>();
    public List<Item> listaImportantes = new List<Item>();
    public List<Item> listaHabilidades = new List<Item>();

    //el inventario en si pa poder hacer cosas con el
    public static Inventario instance;

    //los menuses
    public GameObject EquipablesUI;
    public GameObject ConsumablesUI;
    public GameObject InformacionUI;
    public GameObject ImportantesUI;
    public GameObject HabilidadesUI;

    //cosas del player pa la informacion
    public int MAXHP;
    public int HP;
    public int MAXMANA;
    public int MP;
    public int DAMAGE;
    public int ARMOR;
    public int LVL;
    public int EXP;
    public int ORO;
    public Text TextXP;
    public Text TextLVL;
    public Text TextHP;
    public Text TextMP;
    public Text TextORO;

    void Start()
    {
        HP = Player.HP;
        MAXHP = Player.MAXHP;
        MP = Player.MANA;
        MAXMANA = Player.MAXMANA;
        DAMAGE = Player.DAMAGE;
        ARMOR = Player.ARMOR;
        LVL = Player.LVL;
        EXP = Player.EXP;
        ORO = Player.OROS;

        //los consumables que se le pasan al inventario para que el jugador se recupere cosas
        if (inicial == true)
        {
            Sprite s1 = Resources.Load<Sprite>("Sprites/pociondevida");
            Sprite s2 = Resources.Load<Sprite>("Sprites/Setita");
            Sprite s3 = Resources.Load<Sprite>("Sprites/pan");
            Sprite s4 = Resources.Load<Sprite>("Sprites/carne");
            //Consumable PociondeVida = new Consumable("", "", "", , , );
            listaConsumables.Add(new Consumable("Pocion de Vida", "1", "Poción que recupera 5 puntos de salud.", s1, 5, 0));
            listaConsumables.Add(new Consumable("Pocion de Mana", "1", "Seta que cura 5 puntos de mana.", s2, 0, 5));
            listaConsumables.Add(new Consumable("Pan", "1", "Es pan, cura 2 puntos de vida.", s3, 2, 0));
            listaConsumables.Add(new Consumable("Carne", "1", "Trozo de carne, no apto para veganos, cura 3 puntos de vida.", s4, 3, 0));
            inicial = false;
        }
        instance = this;
        updatePanelSlots();

    }

    //actualiza valores
    void Update()
    {
        HP = Player.HP;
        MP = Player.MANA;
        TextXP.text = EXP + "";
        TextLVL.text = LVL + "";
        TextHP.text = HP + " / " + MAXHP;
        TextMP.text = MP + " / " + MAXMANA;
        TextORO.text = ORO + "";
        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene(lastscene);
        }
    }

    //activar/desactivar menus
    public void ArmasMenu()
    {
        EquipablesUI.SetActive(true);
        ConsumablesUI.SetActive(false);
        InformacionUI.SetActive(false);
        ImportantesUI.SetActive(false);
    }
    //activar/desactivar menus
    public void ConsumablesMenu()
    {
        EquipablesUI.SetActive(false);
        ConsumablesUI.SetActive(true);
        InformacionUI.SetActive(false);
        ImportantesUI.SetActive(false);
    }
    //activar/desactivar menus
    public void HabilidadesMenu()
    {
        EquipablesUI.SetActive(false);
        ConsumablesUI.SetActive(false);
        InformacionUI.SetActive(true);
        ImportantesUI.SetActive(false);
    }
    //activar/desactivar menus
    public void ObjImportantesMenu()
    {
        EquipablesUI.SetActive(false);
        ConsumablesUI.SetActive(false);
        InformacionUI.SetActive(false);
        ImportantesUI.SetActive(true);
    }

    public void updatePanelSlots()
    {
        int i = 0;
        //foreach loop to iterate through the panel's children.
        foreach (Transform child in ConsumablesUI.transform)
        {
            inventorySlotController PanelObjeto = child.GetComponent<inventorySlotController>();
            
            if(i < listaConsumables.Count)
            {
                PanelObjeto.item = listaConsumables[i];
            }
            else
            {
                PanelObjeto.item = null;
            }
            PanelObjeto.updateInfo();
            i++;
        }
        int i2 = 0;
        //foreach loop to iterate through the panel's children.
        foreach (Transform child in EquipablesUI.transform)
        {
            inventorySlotController PanelObjeto = child.GetComponent<inventorySlotController>();

            if (i2 < listaEquipables.Count)
            {
                PanelObjeto.item = listaEquipables[i2];
            }
            else
            {
                PanelObjeto.item = null;
            }
            PanelObjeto.updateInfo();
            i2++;
        }

        int i3 = 0;
        //foreach loop to iterate through the panel's children.
        foreach (Transform child in ImportantesUI.transform)
        {
            inventorySlotController PanelObjeto = child.GetComponent<inventorySlotController>();

            if (i3 < listaImportantes.Count)
            {
                PanelObjeto.item = listaImportantes[i3];
            }
            else
            {
                PanelObjeto.item = null;
            }
            PanelObjeto.updateInfo();
            i3++;
        }

        int i4 = 0;
        //foreach loop to iterate through the panel's children.
        foreach (Transform child in HabilidadesUI.transform)
        {
            inventorySlotController PanelObjeto = child.GetComponent<inventorySlotController>();

            if (i4 < listaHabilidades.Count)
            {
                PanelObjeto.item = listaHabilidades[i4];
            }
            else
            {
                PanelObjeto.item = null;
            }
            PanelObjeto.updateInfo();
            i4++;
        }
    }

    //pone items en la lista, en verdad no lo hemos usado pero ahi esta
    public void Add(Item item)
    {
        if(listaConsumables.Count < 7)
        {
            listaConsumables.Add(item);
        }
        if (this != null)
        {
            updatePanelSlots();
        }
    }

    //quita los items de la lista
    public void Remove(Item item)
    {
        listaConsumables.Remove(item);
        if (this!=null)
        {
            updatePanelSlots();
        }
        
    }
}
