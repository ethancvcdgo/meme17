﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class inventorySlotControllerCombat : MonoBehaviour
{
    //esto es para hacer pruebas, lo usamos de placeholder
    public Item item;


    public PlayerCombat player;

    private void Start()
    {
        updateInfo();
    }

    //usa el objeto que se le pasa
    public void Use()
    {
        if (item)
        {
            //player = FindObjectOfType<PlayerCombat>();
            item.UseCombat(player);
            updateInfo();
        }
    }


    public void updateInfo()
    {
        //esto busca el nombre del objeto de la jerarquia que esta dentro de lo que tiene el script
        Text displayName = transform.Find("NombreObjeto").GetComponent<Text>();
        Image displayImage = transform.Find("ImagenObjeto").GetComponent<Image>();
        Text displayQuantity = transform.Find("CantidadObjeto").GetComponent<Text>();

        //si hay un item enseña sus cosas
        if (item)
        {
            displayName.text = item.itemName;
            displayImage.sprite = item.itemIcon;
            displayQuantity.text = item.itemQuantity;
        }
        //si no no enseña nada
        else
        {
            displayName.text = "";
            displayImage.sprite = null;
            displayQuantity.text = "";
            displayImage.color = Color.clear;
        }
    }
}
