﻿using UnityEngine;

public class musica : MonoBehaviour
{
    //scrip de la musica
    public AudioClip MusicClip;
    public AudioClip MusicClipFondue;
    public AudioSource MusicSource;
    public static bool piano = false;
    // Start is called before the first frame update
    void Start()
    {
        MusicSource.clip = MusicClipFondue;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!piano)
        {
            MusicSource.clip = MusicClipFondue;
        }
        else
        {
            //te playea una cancion una sola vez
            MusicSource.Stop();
            MusicSource.PlayOneShot(MusicClip);
            piano = !piano;
        }
    }





}
