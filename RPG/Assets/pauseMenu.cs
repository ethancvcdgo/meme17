﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

public class pauseMenu : MonoBehaviour
{
    //menu pausa
    public static bool Paused = false;
    public GameObject PauseMenuIU;
    public GameObject TownMenuIU;

    // Update is called once per frame
    void Update()
    {
        //el menu sale cuando pulsas esc
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Paused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
        if (Player.ciudad)
        {
            TownMenuIU.SetActive(true);
            Time.timeScale = 0f;
        }

    }

    //hace que se desactive el menu y el juego vuelva a funcionar
    public void Resume()
    {
        PauseMenuIU.SetActive(false);
        Time.timeScale = 1f;
        Paused = false;
    }

    //hace que se active el menu y el juego se pause
    void Pause()
    {
        PauseMenuIU.SetActive(true);
        Time.timeScale = 0f;
        Paused = true;
    }

    //va hacia el menu principal
    public void GoMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    //Crea el archivo de guardado
    private Save createSave()
    {
        Save save = new Save();
        save.Dinero = Player.OROS;
        save.HP = Player.HP;
        save.MP = Player.MANA;
        save.Scene = SceneManager.GetActiveScene().name;
        save.xC = Player.player.transform.position.x;
        save.yC = Player.player.transform.position.y;
        save.LVL = Player.LVL;
        save.XP = Player.EXP;
        save.lobos = Player.lobos;
        return save;

    }

    //Guarda la informacion
    public void saveGame()
    {
        Debug.Log(Application.persistentDataPath + "/gamesave.save");
        Save save = this.createSave();
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
        bf.Serialize(file, save);
        file.Close();
        Debug.Log("Game Saved");
    }

    //Funcion para cargar la escena de la ciudad e ir a la taberna
    public void GoTown()
    {
        SceneManager.LoadScene("SceneTavern");
        Player.player.transform.position = new Vector3(
            60,
            -7,
            Player.player.transform.position.z);
        Player.ciudad = false;
        Time.timeScale = 1f;

    }

    //Funcion para cargar la escena de la ciudad e ir a la tienda
    public void GoShop()
    {
        SceneManager.LoadScene("SceneTavern");
        Player.player.transform.position = new Vector3(
        22,
        4,
        Player.player.transform.position.z);
        Player.ciudad = false;
        Time.timeScale = 1f;
    }

    //Funcion para cargar la escena de la ciudad e ir al banco
    public void GoBank()
    {
        SceneManager.LoadScene("SceneTavern");
        Player.player.transform.position = new Vector3(
        22,
        23,
        Player.player.transform.position.z);
        Player.ciudad = false;
        Time.timeScale = 1f;

    }
}
