﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gatete : MonoBehaviour
{
    //este script hace que el gato camine hacia abajo, se choque con algo y cambie de direccion hacia arriba
    private bool arriba = false;
    public int vel = 5;
    public Animator animator;
    //this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
    //this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
    // Start is called before the first frame update




    // Update is called once per frame
    void Update()
    {
        if (arriba)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, vel);
            animator.SetBool("arriba", false);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
            animator.SetBool("arriba", true);

        }

    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        arriba = !arriba;

    }
}
