﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{

    public Player player;
    public Camera gameCamera;
    public GameObject grabar;
    public GameObject F;
    public Text score;
    public GameObject disco;
    public GameObject[] blockPrefabs;
    private float puntero;
    private float spawn = 20;
    private int puntos=0;
    public int velOrbit;
    // Start is called before the first frame update
    void Start()
    {
        Instantiate(blockPrefabs[0]);
    }

    // Update is called once per frame
    void Update()
    {
        score.text = "Score: "+puntos;
        score.color = Color.black;
        score.fontSize = 14;
        if (player != null)
        {
            gameCamera.transform.position = new Vector3(
                player.transform.position.x,
                gameCamera.transform.position.y,
                gameCamera.transform.position.z);
            //aumentar y dismunuir velocidad del disco
            if (Input.GetKey("up")&&velOrbit<450)
            {
                velOrbit += 5;
            }
            else if (Input.GetKey("down") && velOrbit > -450)
            {
                velOrbit -= 5;
            }

            //Orbitar sobre player
            Vector3 point = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);
            Vector3 axis = new Vector3(0, 0, 1);
            disco.transform.RotateAround(point, axis, Time.deltaTime*velOrbit);

            //Rotar sobre si mismo
            Vector3 rotacion = new Vector3(0, 0, 1);
            disco.transform.Rotate(rotacion,5f);

            // 4 i 8 para que quede arriba a la izquierda del jugador
            grabar.transform.position = new Vector3(
               player.transform.position.x - 8,
               player.transform.position.y + 4,
               grabar.transform.position.z);
            puntos++;
        }
        else
        {
            //Recargar escena cuando pulsas f y estas muerto
            /*if (Input.GetKeyDown("f"))
            {
                Debug.Log("Score: " + puntos);
                SceneManager.LoadScene("SampleScene");
            }*/
            Debug.Log("Score: " + puntos);
            SceneManager.LoadScene("SampleScene");
        }
        if (player != null && this.puntero < player.transform.position.x + spawn )
        {
            GameObject newBlock = Instantiate(blockPrefabs[Random.Range(1, blockPrefabs.Length)]);
            float size = newBlock.transform.GetChild(0).transform.localScale.x;
            newBlock.transform.position = new Vector2(puntero+size/2, 0);
            puntero += size;
            GameObject.Destroy(newBlock,25f);
        }

    }
    public void masvel()
    {
        if (velOrbit < 450)
        {
            velOrbit += 50;
        }

    }
    public void menosvel()
    {

        if (velOrbit > -450)
        {
            velOrbit -= 50;
        }
    }
}
