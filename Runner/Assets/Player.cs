﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public int jumpForce;
    public int horizontalVelocity;
    public GameObject F;
    private bool salar;
    private bool doblesalo;
    private bool bajar;
    // Start is called before the first frame update


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (this != null)
        {
            //saltar o doble salto
            if ((Input.GetKeyDown("w") || Input.GetKeyDown("space")) && (salar||doblesalo))
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
                if(salar == false)
                {
                    doblesalo = false;
                }
                else
                {
                    salar = false;
                }
            }
            //saltar pero para abajo
            if (Input.GetKeyDown("s")&&bajar)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -jumpForce));
                bajar = false;
            }
            //movimiento horizontal
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(horizontalVelocity, this.GetComponent<Rigidbody2D>().velocity.y);
        }



    }

    public void Saltar()
    {
        if ((salar || doblesalo))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
            if (salar == false)
            {
                doblesalo = false;
            }
            else
            {
                salar = false;
            }
        }
    }
    public void Bajar()
    {
        if (bajar)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -jumpForce));
            bajar = false;
        }
    }
    void OnTriggerEnter2D(Collider2D coll)
    {
        Debug.Log(coll.tag);
        if (coll.tag == "Obstaculo")
        {
            F.transform.position = new Vector3(
                 this.transform.position.x,
                 this.transform.position.y+2,
                 F.transform.position.z);
            GameObject.Destroy(this.gameObject);
            

            
        }
        //buff de velocidad
        if (coll.tag == "+vel")
        {
            this.horizontalVelocity++;
            Debug.Log("La Diosa te anima");
        }
        //debuff de velocidad
        if (coll.tag == "-vel"&&this.horizontalVelocity>4)
        {
            this.horizontalVelocity--;
            Debug.Log("Picachu te ralentiza");
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.gameObject.tag);
        if (collision.gameObject.tag == "Suelo")
        {
            salar = true;
            doblesalo = true;
            bajar = true;
            
        }
    }
}
