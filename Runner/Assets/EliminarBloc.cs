﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EliminarBloc : MonoBehaviour
{
    private int altura = -50;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //elimina bloques que caen muy abajo o bloques que suben mucho
        if ((this.transform.position.y < altura)|| this.transform.position.y > (-altura))
        {
            GameObject.Destroy(this.gameObject);
        }
    }
    //colision con el disco que los elimina
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag=="Disco")
        {
            GameObject.Destroy(this.gameObject);
        }
    }
}
