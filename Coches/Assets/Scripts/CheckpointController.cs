﻿using UnityEngine;

public class CheckpointController : MonoBehaviour
{
    #region Delegates
    public delegate void OnHitPlayerAction(CheckpointController checkpoint, string player);
    #endregion Delegates

    #region Events
    public OnHitPlayerAction OnHitPlayer;
    #endregion Events

    #region Unity Fields
    public int id;
    #endregion Unity Fields

    #region Methods
    public void OnTriggerEnter(Collider collider)
    {
        if (collider.GetComponent<PlayerController>() != null)
        {
            if (OnHitPlayer != null) OnHitPlayer(this, "Player");
        }
        if (collider.GetComponent<PlayerController2>() != null)
        {
            if (OnHitPlayer != null) OnHitPlayer(this, "Player2");
        }
    }
    #endregion Methods
}