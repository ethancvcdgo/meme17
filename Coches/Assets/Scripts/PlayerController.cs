﻿using System;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region Delegates
    public delegate void OnHitSpikeAction();
    public delegate void OnHitOrbAction(string player);
    #endregion Delegates

    #region Events
    public OnHitSpikeAction OnHitSpike;
    public OnHitOrbAction OnHitOrb;
    #endregion Events

    #region Constants
    private float SPEED = 1200f;
    private float ROTATE_SPEED = 20f;
    #endregion Constants

    #region UnityFields
    public GameObject shell;
    #endregion UnityFields

    #region Fields
    private Vector3 leftBound;
    private Vector3 rightBound;
    #endregion Fields

    #region Var
    private bool suelo;
    #endregion Var

    #region Methods
    private void Start()
    {
        //Cambio del centro de masa para abajo para volcar menos(no volcar del todo mientras giras)
        this.gameObject.GetComponent<Rigidbody>().centerOfMass = new Vector3(this.gameObject.GetComponent<Rigidbody>().centerOfMass.x, this.gameObject.GetComponent<Rigidbody>().centerOfMass.y - 2, this.gameObject.GetComponent<Rigidbody>().centerOfMass.z);
    }
    public void FixedUpdate()
    {
        ProcessInput();
    }
    private void ProcessInput()
    {
        if (Input.GetKey("a"))
        {
            /*
             * RotateAround, gira en funció d'un punt (fent servir un punt de referencia, un vector de rotació, i un angle)
             * El punt de referencia seria el centre de l'objecte. si vols rotar sobre tu mateix seria la propia position (recordem que la position implica les coordenades centrals)
             * el vector pots fer servir un dels tres vectors base
             *  transform.up -> gira al voltant de l'eix Y (fletxa verda)
             *  transform.forward -> gira al voltant de l'eix Z (fletxa blava)
             *  transform.right -> gira al voltant de l'eix X (fletxa vermella)
             * */


            /*
             * Time.deltaTime torna el temps entre Frames. D'aquesta forma t'assegures que la velocitat sigui independent del framerate. (un framerate mes baix fara que es mogui mes per frame)
             **/
            transform.RotateAround(transform.position, transform.up, Time.deltaTime * -ROTATE_SPEED);
        }
        if (Input.GetKey("d"))
        {
            transform.RotateAround(transform.position, transform.up, Time.deltaTime * ROTATE_SPEED);
        }

        if (Input.GetKey("w"))
        {
            if (suelo)
            {
                //aplicar fuerzas para moverse
                this.GetComponent<Rigidbody>().AddForce(this.transform.forward * SPEED * Time.deltaTime);
            }
        }
        if (Input.GetKey("s"))
        {
            if (suelo)
            {
                this.GetComponent<Rigidbody>().AddForce(-this.transform.forward * SPEED * Time.deltaTime);
            }

        }
        if (Input.GetKeyDown("q"))
        {
            //disparas un proyectil con rebote infinito
            disparar();
        }
    }

    private void disparar()
    {
        GameObject disparo = Instantiate(shell);
        disparo.transform.forward = this.transform.forward;
        disparo.transform.position = new Vector3(this.transform.position.x + this.transform.forward.x * 20, this.transform.position.y + 8, this.transform.position.z + this.transform.forward.z * 20);
        disparo.transform.localRotation = this.transform.localRotation;
        disparo.GetComponent<Rigidbody>().AddForce(disparo.transform.forward*2000);
        Destroy(disparo.gameObject, 5f);
    }

    //Si no estas en el suelo, moverse no
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.name.Equals("Suleo"))
        {
            suelo = true;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.name.Equals("Suleo"))
        {
            suelo = false;
        }
    }
    #endregion Methods
}