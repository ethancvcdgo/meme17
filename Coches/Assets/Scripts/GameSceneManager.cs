﻿using UnityEngine;
using UnityEngine.UI;

public class GameSceneManager : MonoBehaviour
{
    #region Unity Fields
    public Camera mainCamera;
    public PlayerController player;
    public Camera mainCamera2;
    public PlayerController2 player2;
    public Text scoreText;
    public Text gameOverText;
    public Text lapText;
    public Text scoreText2;
    public Text gameOverText2;
    public Text lapText2;
    public Transform checkpointContainer;
    public Light luh;
    public float distlargocamara;
    public float distaltocam;
    #endregion Unity Fields
    #region Fields
    private int score;
    private float gameTimer;
    private int nCheckpoints;
    private int currentCheckpoint;
    private float trackStartTime;
    private bool started;
    private int nLaps;
    private float totTime;
    private int score2;
    private float gameTimer2;
    private bool gameOver2;
    private int currentCheckpoint2;
    private float trackStartTime2;
    private int nLaps2;
    private float totTime2;
    public BestoTimeLap bt;
    public BestoTimeLap bt2;
    #endregion Fields
    #region Methods
    public void Start()
    {
        Time.timeScale = 1;
        //player.OnHitSpike += OnGameOver;
        player.OnHitOrb += OnGameWin;
        player2.OnHitOrb += OnGameWin;
        nCheckpoints = checkpointContainer.childCount;
        foreach (CheckpointController checkpoint in checkpointContainer.GetComponentsInChildren<CheckpointController>())
        {
            checkpoint.OnHitPlayer += OnReachCheckpoint;
        }

        currentCheckpoint = -1;
        nLaps = 0;
        totTime = 0;

        scoreText.text = "";
        lapText.text = "";

        currentCheckpoint2 = -1;
        nLaps2 = 0;
        totTime2 = 0;

        scoreText2.text = "";
        lapText2.text = "";
    }

    public void Update()
    {
        // LERP fa una interpolació lineal. Aixo fara un moviment de camara molt més smooth que no si directament ho posem a la posició del jugador
        mainCamera.transform.position = new Vector3
        (
            //volem la posició no exactament en el jugador, sino que la volem una mica enrere i damunt seu. Si el jugador gira la camara s'hauria de moure respecte al seu nou darrere, orbitant
            Mathf.Lerp(mainCamera.transform.position.x, (player.transform.position.x - (player.transform.forward.x * distlargocamara)), Time.deltaTime * 10),
            Mathf.Lerp(mainCamera.transform.position.y, player.transform.position.y + distaltocam, Time.deltaTime * distlargocamara),
            Mathf.Lerp(mainCamera.transform.position.z, (player.transform.position.z - (player.transform.forward.z * distlargocamara)), Time.deltaTime * 10)
        );

        //fa una rotació automàtica per a que miri al jugador
        mainCamera.transform.LookAt(player.transform);
        mainCamera2.transform.position = new Vector3
        (
            //volem la posició no exactament en el jugador, sino que la volem una mica enrere i damunt seu. Si el jugador gira la camara s'hauria de moure respecte al seu nou darrere, orbitant
            Mathf.Lerp(mainCamera2.transform.position.x, (player2.transform.position.x - (player2.transform.forward.x * distlargocamara)), Time.deltaTime * 10),
            Mathf.Lerp(mainCamera2.transform.position.y, player2.transform.position.y + distaltocam, Time.deltaTime * distlargocamara),
            Mathf.Lerp(mainCamera2.transform.position.z, (player2.transform.position.z - (player2.transform.forward.z * distlargocamara)), Time.deltaTime * 10)
        );

        //fa una rotació automàtica per a que miri al jugador
        mainCamera2.transform.LookAt(player2.transform);
        GiroLuh();
        if (started)
        {
            scoreText.text = "Time: " + (Time.time - trackStartTime).ToString("F2");
            scoreText2.text = "Time: " + (Time.time - trackStartTime2).ToString("F2");
            lapText.text = "Laps: " + (nLaps + 1) + "/3";
            lapText2.text = "Laps: " + (nLaps2 + 1) + "/3";
            if (bt.bestTime > 0)
            {
                scoreText.text += "\nBest: " + (bt.bestTime.ToString("F2"));
            }
            if (bt2.bestTime > 0)
            {
                scoreText2.text += "\nBest: " + (bt2.bestTime.ToString("F2"));
            }
        }
    }
    //LA luh rota para hacer efecto dia noche
    private void GiroLuh()
    {
        luh.transform.transform.RotateAround(luh.transform.position, luh.transform.up, Time.deltaTime * 10);
    }
    //Pones etiquetas y quitas y uno de los jugadores gana, el otro pierde
    public void OnGameWin(string player)
    {

        scoreText.enabled = false;
        lapText.enabled = false;
        scoreText.enabled = false;
        lapText.enabled = false;
        scoreText2.enabled = false;
        lapText2.enabled = false;
        gameOverText2.enabled = true;
        gameOverText.enabled = true;
        gameOverText2.enabled = true;
        if (player.Equals("Player"))
        {
            gameOverText.text = "Has ganado";
            gameOverText2.text = "Has Perdido";
        }
        else
        {
            gameOverText.text = "Has perdido";
            gameOverText2.text = "Has ganado";
        }
        Time.timeScale = 0;
    }

    //Cuando llegas a un punto de control mira el anterior y si bien pues sigue
    private void OnReachCheckpoint(CheckpointController checkpoint, string player)
    {
        if (player.Equals("Player"))
        {
            if (currentCheckpoint == -1 && checkpoint.id == 0)
            {
                started = true;
                trackStartTime = Time.time;
                currentCheckpoint = checkpoint.id;
            }
            else if (checkpoint.id == 0 && currentCheckpoint == nCheckpoints - 1)
            {
                OnFullTrack("Player");
                currentCheckpoint = checkpoint.id;
            }
            else if (checkpoint.id == currentCheckpoint + 1)
            {
                currentCheckpoint = checkpoint.id;
            }
        }
        else
        {
            if (currentCheckpoint2 == -1 && checkpoint.id == 0)
            {
                started = true;
                trackStartTime2 = Time.time;
                currentCheckpoint2 = checkpoint.id;
            }
            else if (checkpoint.id == 0 && currentCheckpoint2 == nCheckpoints - 1)
            {
                OnFullTrack("Player2");
                currentCheckpoint2 = checkpoint.id;
            }
            else if (checkpoint.id == currentCheckpoint2 + 1)
            {
                currentCheckpoint2 = checkpoint.id;
            }
        }
    }

    //Cuando haces una vuelta pues vuelta hecha
    private void OnFullTrack(string player)
    {
        if (player.Equals("Player"))
        {
            float currentTime = Time.time - trackStartTime;
            if (bt.bestTime == 0 || currentTime < bt.bestTime)
            {
                bt.bestTime = currentTime;
            }
            totTime += currentTime;
            trackStartTime = Time.time;
            nLaps++;
            if (nLaps == 3)
            {
                this.OnGameWin(player);
            }
        }
        else
        {
            float currentTime = Time.time - trackStartTime2;
            if (bt2.bestTime == 0 || currentTime < bt2.bestTime)
            {
                bt2.bestTime = currentTime;
            }
            totTime2 += currentTime;
            trackStartTime2 = Time.time;
            nLaps2++;
            if (nLaps2 == 3)
            {
                this.OnGameWin(player);
            }
        }
    }
    #endregion Methods
}