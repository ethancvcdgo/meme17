﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Best Lap", menuName = "Lap Data", order = 51)]
public class BestoTimeLap : ScriptableObject
{
    [SerializeField]
    public float bestTime;
}
