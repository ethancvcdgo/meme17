﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GodController : MonoBehaviour
{
    //Cosa a distancia para atacar
    public GameObject Rayito;
    //Estadio del combo
    public int status;
    //combo roto
    private bool cBreak;
    //Si estoy pegando o no
    private bool pego;
    //Para saltar con suelo
    private bool Sallto = true;
    //Puntos Vida
    public float vida = 100;
    //Si estas bloqueando
    private bool bloqueo;
    //Texcto de victoria
    public Text NegroWins;
    //Delegado para bajar barras de vida
    public delegate void OnDamaged(float vida);
    public event OnDamaged OnDamagedEvent;

    // Start is called before the first frame update
    void Start()
    {
        cBreak = true;
    }

    // Update is called once per frame
    void Update()
    {
        //Si no vida te mueres y gana el otro
        if (vida <= 0)
        {
            this.GetComponent<Animator>().SetBool("Bloqueo", false);
            this.GetComponent<Animator>().SetBool("Move", false);
            this.GetComponent<Animator>().SetTrigger("Morido");
            Debug.Log("NegroWins");
            NegroWins.gameObject.SetActive(true);
            Destroy(this);
        }
        else
        {
            //Caerse del mapa igual morir
            if (this.transform.position.y < -50)
            {
                vida = 0;
                OnDamagedEvent(vida);
            }
            //Ataque normal
            if (Input.GetKeyDown("[1]"))
            {
                if (Sallto && !bloqueo)
                {
                    if (status == 1)
                    {
                        pego = true;
                        GameObject ataque = this.gameObject.transform.GetChild(0).gameObject;
                        ataque.SetActive(true);
                        this.GetComponent<Animator>().SetBool("Move", false);
                        this.GetComponent<Animator>().SetTrigger("Atac2");
                        StartCoroutine(PegarParadaNormal(0.9f, ataque));
                    }
                    else
                    {
                        cBreak = true;
                        status = 0;
                    }
                    if (status == 0 && !pego)
                    {
                        pego = true;
                        GameObject ataque = this.gameObject.transform.GetChild(0).gameObject;
                        ataque.SetActive(true);
                        this.GetComponent<Animator>().SetBool("Move", false);
                        this.GetComponent<Animator>().SetTrigger("Atac1");
                        cBreak = false;
                        StartCoroutine(MaxCombo(0.5f, status));
                        StartCoroutine(MinCombo(0.2f, 1));
                        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
                        StartCoroutine(PegarParadaNormal(0.8f, ataque));
                    }
                }
            }
            else if (Input.GetKeyDown("[2]") && !pego && Sallto && !bloqueo)
            {
                //Ataque a distancia
                pego = true;
                this.GetComponent<Animator>().SetTrigger("Rayo");
                GameObject rayo = Instantiate(Rayito);
                if (this.transform.localScale.x == 1.2f)
                {
                    rayo.transform.position = new Vector2(this.transform.position.x - 5, this.transform.position.y - 1f);
                }
                else
                {
                    rayo.transform.position = new Vector2(this.transform.position.x + 5, this.transform.position.y - 1f);
                }
                Destroy(rayo.gameObject, 1f);
                StartCoroutine(PegarParada(0.8f));
            }
            else if (Input.GetKey("right") && !pego && !bloqueo)
            {
                this.GetComponent<Animator>().SetBool("Move", true);
                this.transform.localScale = new Vector2(-1.2f, 1.2f);
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(4, this.GetComponent<Rigidbody2D>().velocity.y);
            }
            else if (Input.GetKey("left") && !pego && !bloqueo)
            {
                this.GetComponent<Animator>().SetBool("Move", true);
                this.transform.localScale = new Vector2(1.2f, 1.2f);
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-4, this.GetComponent<Rigidbody2D>().velocity.y);
            }
            else
            {
                this.GetComponent<Animator>().SetBool("Move", false);
            }
            //salto
            if (Input.GetKeyDown("up") && Sallto)
            {
                this.GetComponent<Animator>().SetTrigger("Dsalt");
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 350));
                this.GetComponent<Animator>().SetBool("Move", false);
                Sallto = false;
            }
            else if (Input.GetKey("down"))
            {
                //bloqueo
                bloqueo = true;
                this.GetComponent<Animator>().SetBool("Bloqueo", true);
            }
            else
            {
                this.GetComponent<Animator>().SetBool("Bloqueo", false);
                bloqueo = false;
            }
        }
    }
    //Al rato de cuando pegas te cambia el boolean para que puedas volver a pegar
    IEnumerator PegarParadaNormal(float v, GameObject ataque)
    {
        yield return new WaitForSeconds(v);
        if (pego == true)
        {
            pego = false;
        }
    }
    //Cosas del combos
    IEnumerator MinCombo(float f, int st)
    {
        yield return new WaitForSeconds(f);
        if (!cBreak)
        {
            status = st;
        }

    }
    IEnumerator MaxCombo(float f, int st)
    {
        yield return new WaitForSeconds(f);
        if (status != st)
        {
            status = 0;
            cBreak = true;
        }
    }
    IEnumerator PegarParada(float f)
    {
        yield return new WaitForSeconds(f);
        if (pego == true)
        {
            pego = false;
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Suelo")
        {
            //poder no saltar infinito
            Sallto = true;
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //comerse daño con ataques y saltito de daño
        if (collision.gameObject.CompareTag("Hacha") && !bloqueo)
        {
            if (this.transform.localScale.x == 1.2f)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(450f, 150f));
            }
            else
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-450f, 150f));
            }
            Destroy(collision.gameObject);
            vida -= 10;
            if (vida <= 0)
            {
                vida = 0;
            }
            OnDamagedEvent(vida);
        }
        //Hurtbox del otro personaje ataques normal
        if (collision.gameObject.name == "DanoZona" && !bloqueo)
        {
            if (this.transform.localScale.x == 1.2f)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(450f, 150f));
            }
            else
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-450f, 150f));
            }
            vida -= 5;
            if (vida <= 0)
            {
                vida = 0;
            }
            OnDamagedEvent(vida);
        }
    }
}
