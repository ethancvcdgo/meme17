﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class NegroController : MonoBehaviour
{
    //Comentacion esta en el otro personaje porque mas o menos igual
    public GameObject hacha;
    public int status;
    private bool cBreak;
    private bool pego;
    private bool bloqueo;
    private bool Sallto = true;
    public float vida = 100;
    public Text GodWins;

    public delegate void OnDamaged(float vida);
    public event OnDamaged onDamagedEvent;


    // Start is called before the first frame update
    void Start()
    {
        cBreak = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (vida <= 0)
        {
            this.GetComponent<Animator>().SetBool("Bloqueo", false);
            this.GetComponent<Animator>().SetBool("Caminar", false);
            this.GetComponent<Animator>().SetTrigger("Morido");
            Debug.Log("GodWins");
            GodWins.gameObject.SetActive(true);
            Destroy(this);
        }
        else
        {
            if (this.transform.position.y < -50)
            {
                vida = 0;
                onDamagedEvent(vida);
            }
            if (Input.GetKeyDown("n"))
            {
                if (Sallto)
                {
                    if (!bloqueo)
                    {
                        if (status == 1)
                        {
                            pego = true;
                            GameObject ataque = this.gameObject.transform.GetChild(0).gameObject;
                            ataque.SetActive(true);
                            this.GetComponent<Animator>().SetBool("Caminar", false);
                            this.GetComponent<Animator>().SetTrigger("Atac2");
                            StartCoroutine(PegarParadaNormal(1f, ataque));
                        }
                        else
                        {
                            cBreak = true;
                            status = 0;
                        }
                        if (status == 0 && !pego)
                        {
                            pego = true;
                            GameObject ataque = this.gameObject.transform.GetChild(0).gameObject;
                            ataque.SetActive(true);
                            this.GetComponent<Animator>().SetBool("Caminar", false);
                            this.GetComponent<Animator>().SetTrigger("Atac1");
                            cBreak = false;
                            StartCoroutine(MaxCombo(0.5f, status));
                            StartCoroutine(MinCombo(0.2f, 1));
                            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
                            StartCoroutine(PegarParadaNormal(0.8f, ataque));
                        }
                    }
                }
                else
                {
                    pego = true;
                    this.GetComponent<Animator>().SetBool("Caminar", false);
                    this.GetComponent<Animator>().SetTrigger("VolPat");
                    StartCoroutine(PegarParada(0.35f));
                }
            }
            else if (Input.GetKeyDown("b") && !pego && !bloqueo)
            {
                pego = true;
                this.GetComponent<Animator>().SetTrigger("HachaLanza");
                GameObject newHacha = Instantiate(hacha);
                newHacha.transform.position = this.transform.position + new Vector3(0, 1, 0);
                if (this.transform.localScale.x == 1.5f)
                {
                    //Hacha gira y eso
                    newHacha.GetComponent<Rigidbody2D>().AddForce(new Vector2(200, 400));
                    newHacha.transform.localScale = new Vector2(-1f, 1f);
                    newHacha.GetComponent<Rigidbody2D>().AddTorque(800);
                }
                else
                {
                    newHacha.GetComponent<Rigidbody2D>().AddForce(new Vector2(-200, 400));
                    newHacha.transform.localScale = new Vector2(1f, 1f);
                    newHacha.GetComponent<Rigidbody2D>().AddTorque(-800);
                }
                Destroy(newHacha.gameObject, 4.5f);
                StartCoroutine(PegarParada(0.8f));
            }
            else if (Input.GetKey("d") && !pego && !bloqueo)
            {
                this.GetComponent<Animator>().SetBool("Caminar", true);
                this.transform.localScale = new Vector2(1.5f, 1.5f);
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(4, this.GetComponent<Rigidbody2D>().velocity.y);
            }
            else if (Input.GetKey("a") && !pego && !bloqueo)
            {
                this.GetComponent<Animator>().SetBool("Caminar", true);
                this.transform.localScale = new Vector2(-1.5f, 1.5f);
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-4, this.GetComponent<Rigidbody2D>().velocity.y);
            }
            else
            {
                this.GetComponent<Animator>().SetBool("Caminar", false);
            }
            if (Input.GetKeyDown("w") && Sallto)
            {
                this.GetComponent<Animator>().SetTrigger("Dsalt");
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 350));
                this.GetComponent<Animator>().SetBool("Caminar", false);
                Sallto = false;
            }
            else if (Input.GetKey("s"))
            {
                //bloqueo
                bloqueo = true;
                this.GetComponent<Animator>().SetBool("Bloqueo", true);
            }
            else
            {
                this.GetComponent<Animator>().SetBool("Bloqueo", false);
                bloqueo = false;
            }
        }
    }

    IEnumerator PegarParadaNormal(float v, GameObject ataque)
    {
        yield return new WaitForSeconds(v);
        if (pego == true)
        {
            pego = false;
        }
    }
    IEnumerator MinCombo(float f, int st)
    {
        yield return new WaitForSeconds(f);
        if (!cBreak)
        {
            status = st;
        }

    }
    IEnumerator MaxCombo(float f, int st)
    {
        yield return new WaitForSeconds(f);
        if (status != st)
        {
            status = 0;
            cBreak = true;
        }
    }
    IEnumerator PegarParada(float f)
    {
        yield return new WaitForSeconds(f);
        if (pego == true)
        {
            pego = false;
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Suelo")
        {
            Sallto = true;
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0,this.GetComponent<Rigidbody2D>().velocity.y);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Rayo"))
        {
            if (this.transform.localScale.x == 1.5f)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-350f, 150f));
            }
            else
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(350f, 150f));
            }
            vida -= 12;
            if (vida <= 0)
            {
                vida = 0;
            }
            onDamagedEvent(vida);
        }
        if (collision.gameObject.name == "DanoZona")
        {
            if (this.transform.localScale.x == 1.5f)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-350f, 100f));
            }
            else
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(350f, 100f));
            }
            vida -= 8;
            if (vida <= 0)
            {
                vida = 0;
            }
            onDamagedEvent(vida);
        }
    }
}
