﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidaControlle : MonoBehaviour
{
    //Personaje Dios
    public GodController god;
    //Personaje no dios
    public NegroController negro;
    //Vida Dios
    public GameObject VidaGod;
    //Vida De persona sin derechos
    public GameObject VidaNegro;

    // Start is called before the first frame update
    void Start()
    {
        god.OnDamagedEvent +=GodDamage;
        negro.onDamagedEvent +=NegroDamage;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //Cambia la vida en el HUID
    void GodDamage(float hp)
    {
        VidaGod.transform.localScale = new Vector2(hp / 100, VidaGod.transform.localScale.y);
    }
    void NegroDamage(float hp)
    {
        VidaNegro.transform.localScale = new Vector2(hp / 100, VidaNegro.transform.localScale.y);
    }
}
